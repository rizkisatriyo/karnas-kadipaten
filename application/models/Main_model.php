<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Main_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    
   function list($table)
    {
        
      $this->db->select("*");
      $this->db->order_by('id', 'desc');
      $result = $this->db->get($table);

      if($result->num_rows()>0)
      return $result->result();
      else
      return "empty";

    }

    function jumlah_data($table){
		return $this->db->get($table)->num_rows();
	}

    function get($table, $slug)
    {

        $this->db->select("*");
        $this->db->where('slug',$slug);
        $result = $this->db->get($table);
        if($result->num_rows()>0)
        return $result->result();
        else
        return 'empty';
        

    }

    //tambah data
    public function insert($table, $data = array())
    {
        if(!empty($data) && is_array($data))
        {
            $this->db->set($data);
            $this->db->insert($table);
        }
    }   

    // delete
   public function delete($table, $id = null)
     {
         if(!empty($id))
         {
             $this->db->where('id', $id);
             $this->db->delete($table);
         }
     }



    //edit     
    public function update($table, $id, $data = array())
        {
            if(!empty($id) && !empty($data) && is_array($data))
            {
                $this->db->where('id', $id);
                $this->db->update($table);
            }
        }

    // counter views
    // field count_view harus ada di table database
    function update_counter($table, $slug) 
    {
        // return current article views 
        $this->db->where('slug', urldecode($slug));
        $this->db->select('count_view');
        $count = $this->db->get($table)->row();
        // then increase by one 
        $this->db->where('slug', urldecode($slug));
        $this->db->set('count_view', ($count->count_view + 1));
        $this->db->update($table);
    }

    function listImage($slug)
    {

      $this->db->select("*");
      $this->db->from('tbl_portfolio');
      $this->db->join('tbl_image','tbl_image.id_post = tbl_portfolio.id');
      $this->db->where('tbl_portfolio.slug', $slug);
      $result = $this->db->get();

      if($result->num_rows()>0)
      return $result->result();
      else
      return "empty";
    }


    public function listing($table) {
        $this->db->select('*');
        $query = $this->db->get($table);
        return $query->row_array();
    }

    function getSetting($table, $id)
      {

          $this->db->select("*");
          $this->db->where('id', 1);
          $result = $this->db->get($table);
          if($result->num_rows()>0)
          return $result->result();
          else
          return 'empty';

      }



}
