    <!-- <section>
        <div id="carouselExampleIndicators" class="carousel slide">
            <div class="carousel-indicators">
			<?php
                $key = 0;
            ?>
            <?php foreach ($data_slide as $key => $item) {?>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="<?=$key ?>" class="<?=$key == 0 ? 'active' : '' ?>"
                aria-current="true" aria-label="Slide 1"></button>
                <?php
					$key++; }
				?>
                
            </div>
            <div class="carousel-inner">
				<?php foreach ($data_slide as $key => $item) : ?>
					<?php 
						if($item['f_photo'] != "") { 
						$active = ($key == 0) ? 'active' : '';
					?>
					<div class="carousel-item <?=$active ?>">
						<img src="<?php echo base_url_assets() ?>files/<?= $item['f_photo'] ?>" class="d-block" alt="...">
					</div>
					<?php } ?>
            	<?php endforeach ?>
                
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
	</section> -->
    <section>
        <div id="carouselExampleIndicators" class="carousel slide">
            <div class="carousel-indicators">	
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                aria-current="true" aria-label="Slide 1"></button>
            </div>
            <div class="carousel-inner">
				
					<div class="carousel-item active">
						<img src="<?php echo base_url() ?>assets/images/karnasciamis.png" class="d-block" alt="...">
					</div>

            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
	</section>	
	
	<section class="py-50 bg-white" data-aos="fade-up">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-lg-6 col-12 position-relative">
					<div class="popup-vdo mt-30 mt-md-0 text-center">
                        <img src="<?php echo base_url('assets');?>/images/edwan.png" alt="About"  width="70%">

					</div>
				</div>
				<div class="col-lg-6 col-12">
                    <h4 class="fw-400">
                        Assalamualaikum  Warahmatullahhi Wabarokatuh
                    </h4>
                    <p class="fs-16">
                        Kita panjatkan puji syukur ke hadirat Allah SWT yang telah memberikan kita kesehatan dan panjang umur sehingga kita dapat melaksanakan rutinitas sehari-hari, sholawat serta salam semoga selalu tercurah limpahkan kepada junjungan kita nabi besar kita yakni nabi Muhammad SAW beserta keluarganya, sahabat-sahabatnya dan semoga sampai pada kita selaku umatnya, aamiin.
                        <br>
                        <br>
                    Akhir kata tak lupa saya ucapkan terima kasih , semoga website ini menjadi lebih berguna dan bermanfaat.

                    <br>
                    <h4 class="fw-400">
                        Wassalamu'alaikum wr.wb.
                    </h4>
                    </p>
                    <hr>
                    <p class="text-end">
                    <i>H. Edwan Gustiawan, S.T<br>Kepala Sekolah</i>
                    <!-- <i>DR. H. Yepri Esa Trijaka, M.M.Pd<br>Kepala Sekolah</i> -->
                    </p>
					
				</div>
			</div>
		</div>
	</section>

        
    <section class="py-50 bg-img countnm-bx" style="background-image: url(<?php echo base_url('assets') ?>/images/front-end-img/background/bg-9.png)" data-overlay="3" data-aos="fade-up">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-12">
					<div class="text-center">
						<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center mx-auto">
							<span class="text-white fs-40 icon-User"><span class="path1"></span><span class="path2"></span></span>
						</div>
						<h1 class="countnm my-10 text-white fw-300">12</h1>
						<div class="text-uppercase text-white">Guru</div>
					</div>
				</div>	
				<div class="col-lg-3 col-md-6 col-12">
					<div class="text-center">
						<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center mx-auto">
							<span class="text-white fs-40 icon-Book"></span>
						</div>
						<h1 class="countnm my-10 text-white fw-300">120</h1>
						<div class="text-uppercase text-white">Mapel</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<div class="text-center">
						<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center mx-auto">
							<span class="text-white fs-40 icon-Group"><span class="path1"></span><span class="path2"></span></span>
						</div>
						<h1 class="countnm my-10 text-white fw-300">231</h1>
						<div class="text-uppercase text-white">Siswa</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<div class="text-center">
						<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center mx-auto">
							<span class="text-white fs-40 icon-Globe"><span class="path1"></span><span class="path2"></span></span>
						</div>
						<h1 class="countnm my-10 text-white fw-300"></h1>
						<div class="text-uppercase text-white">Alumni</div>
					</div>
				</div>			
			</div>
		</div>
	</section>
	

    <!--Page content -->
	<section class="py-50" data-aos="fade-up">
		<div class="container">
			<div class="row">
				<div class="col-xl-4 col-md-5 col-sm-12">					
					<div class="course-detail-bx">
						<!-- <div class="box box-body">
                            <div class="widget clearfix">
                                <h4 class="pb-15 mb-15 bb-1">Arsip 2023</h4>
                                <div class="media-list media-list-divided">
                                    <a class="px-0 media media-single" href="#">
                                    <span class="title ms-0">Januari </span>
                                    <span class="mx-0 badge badge-secondary-light">125</span>
                                    </a>

                                    <a class="px-0 media media-single" href="#">
                                    <span class="title ms-0">Februari</span>
                                    <span class="mx-0 badge badge-primary-light">124</span>
                                    </a>

                                    <a class="px-0 media media-single" href="#">
                                    <span class="title ms-0">Maret</span>
                                    <span class="mx-0 badge badge-info-light">425</span>
                                    </a>

                                    <a class="px-0 media media-single" href="#">
                                    <span class="title ms-0">April</span>
                                    <span class="mx-0 badge badge-success-light">321</span>
                                    </a>

                                    <a class="px-0 media media-single" href="#">
                                    <span class="title ms-0">Mei</span>
                                    <span class="mx-0 badge badge-danger-light">159</span>
                                    </a>

                                    <a class="px-0 media media-single" href="#">
                                    <span class="title ms-0">Juni</span>
                                    <span class="mx-0 badge badge-warning-light">452</span>
                                    </a>
                                </div>
                            </div>
						</div> -->
						<div class="box box-body">
                            <h4 class="mb-20">Tautan</h4>
                            <ul class="list-unstyled">
			 				    <li><a href="http://edmodo.com" target="_blank">EDMODO LEARNING</a></li>
                                <li><a href="http://kuningankab.go.id" target="_blank">Website Kab. Kuningan</a></li>
                                <li><a href="http://www.uniku.ac.id" target="_blank">Universitas Kuningan</a></li>
                                <li><a href="http://sim.smkkarnas.sch.id/" target="_blank">Agenda Mengajar dan Absensi Siswa Online</a></li>
                                <li><a href="http://sika.smkkarnas.sch.id/" target="_blank">Manajemen Kartu Online</a></li>
                                <li><a href="http://unbk.kemdikbud.go.id" target="_blank">WEB UNBK KEMDIKBUD</a></li>
                                <li><a href="http://siapschool.com" target="_blank">SIAP SCHOOL</a></li>
                                <li><a href="http://smkkarnas.sch.id" target="_blank">Status Jaringan Lokal</a></li>
                                <li><a href="https://www.youtube.com/watch?v=NySGBB2pMt8" target="_self">Video Vocational Challenge 2021</a></li>						</ul>

                        </div>
						<div class="box box-body">
                            <h4 class="mb-20">Hubungi Kami</h4>
		
                            <div>								
                                <div class="d-flex align-items-center mb-5">
                                    <div class="bg-primary rounded h-30 w-30 l-h-30 text-center me-10">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                    <a href="">smkkarnascms@gmail.com</a>
                                </div>							
                                <div class="d-flex align-items-center mb-5">
                                    <div class="bg-primary rounded h-30 w-30 l-h-30 text-center me-10">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <a href="#"> (0265) 7521244</a>
                                </div>						
                                <div class="d-flex align-items-center mb-5">
                                    <div class="bg-primary rounded h-30 w-30 l-h-30 text-center me-10">
                                        <i class="fa fa-map"></i>
                                    </div>
                                    <a href="#"> Jl. Raya Rajapolah – Sindangkasih RT.09/RW.05 Kab. Ciamis</a>
                                </div>
                            </div>
                               
						</div>
                        <div class="box box-body">
							<h4 class="mb-20">Ikuti Kami</h4>
                            <div class="social-icons">
                                <ul class="list-unstyled d-flex gap-items-1 ">
                                    <li><a href="https://www.instagram.com/smkkarnasciamissindangkasih/" class="waves-effect waves-circle btn btn-social-icon btn-circle btn-instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="https://www.facebook.com/groups/164212590443247/" class="waves-effect waves-circle btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://www.instagram.com/smkkarnassindangkasih/" class="waves-effect waves-circle btn btn-social-icon btn-circle btn-twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.youtube.com/channel/UCV0aDDAVdqlyYGCWtSPRhIg" class="waves-effect waves-circle btn btn-social-icon btn-circle btn-youtube"><i class="fa fa fa-youtube"></i></a></li>
                                </ul>
                            </div>

                        </div>

					</div>
				</div>
				<div class="col-xl-8 col-md-7 col-12">
                    <div class="row">
                        <div class="col">
                            <h4 class="page-title mb-25 fw-500">Tulisan terbaru</h4>	
                        </div>
                        <div class="col">
                            <a href="#"><h4 class="text-end">Lainnya</h4></a>
                        </div>
                    </div>
					<?php foreach ($data_news as $item) : ?>
                    <div class="box">
					  <div class="row g-0">
						<div class="col-md-4 col-12 bg-img h-md-auto h-250" style="background-image: url(<?php echo base_url_assets(); ?>news/<?= $item['f_photo'] ?>)"></div>
						<div class="col-md-8 col-12">
						  <div class="box-body">
							<h4><a href="<?php base_url()?>website/blogDetail/?id=<?= $item['_id']?>"><?=$item['title'] ?></a></h4>
							<div class="d-flex mb-10">
							  	<div class="me-10">
									<i class="fa fa-user me-5"></i> <?= $item['f_created_by'] ?>
								</div>
							  	<div>
									<i class="fa fa-calendar me-5"></i> <?= $item['f_created_on'] ?>
								</div>
							</div>

							<div class="flexbox align-items-center mt-3">
							  <a class="btn btn-sm btn-primary" href="<?php base_url()?>website/blogDetail/?id=<?= $item['_id']?>">Read more</a>

							  <div class="gap-items-4">
								<a class="text-muted" href="#">
								  <i class="fa fa-heart me-1"></i> 25
								</a>
								<a class="text-muted" href="#">
								  <i class="fa fa-comment me-1"></i> 23
								</a>
							  </div>
							</div>
						  </div>
						</div>
					  </div>
					</div>
					<?php endforeach ?>


					
					<div class="box">
                        <div class="box-header text-center no-border">
                            <h2 class="box-title">Galeri Foto & Video</h2>
                        </div>
                        <div class="box-body pt-0">
                            <ul class="nav nav-tabs justify-content-center bb-0 mb-10" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-bs-toggle="tab" href="#home12" role="tab">All</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-bs-toggle="tab" href="#foto" role="tab">Foto</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-bs-toggle="tab" href="#video" role="tab">Video</a> </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home12" role="tabpanel">
                                    <div class="px-15 pt-15">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-6 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/4.jpg" alt="Card image cap">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/6.jpg" alt="Card image cap">
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/5.jpg" alt="Card image cap">
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/2.jpg" alt="Card image cap">
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="foto" role="tabpanel">
                                    <div class="px-15 pt-15">
                                        <div class="row">
                                            <div class="col-md-4 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/9.jpg" alt="Card image cap">
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/10.jpg" alt="Card image cap">
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/6.jpg" alt="Card image cap">
                                                    </a>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="video" role="tabpanel">
                                    <div class="px-15 pt-15">
                                        <div class="row">
                                            <div class="col-md-4 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/7.jpg" alt="Card image cap">
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/8.jpg" alt="Card image cap">
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-12">
                                                <div class="box">
                                                    <a href="#">
                                                        <img class="card-img-top" src="<?php echo base_url('assets');?>/images/front-end-img/courses/9.jpg" alt="Card image cap">
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <div class="box box-body">
							<div class="widget mb-0">
								<h4 class="mb-20">Apa kata Mereka?</h4>
								<div class="owl-carousel" data-nav-dots="false" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
                               
                                    <?php foreach ($data_testimoni as $item) : ?>
                                    <div class="item">
										<div class="testimonial-widget">
											<div class="testimonial-content">
												<p><?= $item['contents'] ?></p>
											</div>
											<div class="testimonial-info mt-20">
												<div class="testimonial-avtar">
													<img class="img-fluid " src="<?php echo base_url_assets(); ?>testimoni/<?= $item['f_photo'] ?>" alt="" width="10px">
												</div>
												<div class="testimonial-name">
													<strong><?= $item['audience'] ?></strong>
													<span>Siswa</span>
												</div>
											</div>
										</div>
									</div>
                                    <?php endforeach ?>

								</div>						
							</div>
						</div>
				</div>				
			</div>
		</div>
	</section>