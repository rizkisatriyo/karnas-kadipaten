    <!---page Title --->
	<section class="bg-img pt-120 " data-overlay="4" style="background-image: url(<?php echo base_url('assets') ?>/images/front-end-img/background/bg-9.png)">
			<div class="container">
				<div class="row">
					<div class="col-12">    
						<div class="text-center">						
							
						</div>
					</div>
				</div>
			</div>
		</section>
    <section class="py-50">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-7 col-12">
					<form class="contact-form" action="">
						<div class="text-start mb-30">
							<h2>Hubungi Kami</h2>
							<p>Silakan bertanya kepada kami</p>
						</div>
						<div class="row">
						  <div class="col-md-12">
							<div class="form-group">
							  <input type="text" class="form-control" placeholder="Nama">
							</div>
						  </div>
						  
						  <div class="col-md-6">
							<div class="form-group">
							  <input type="email" class="form-control" placeholder="Email">
							</div>
						  </div>
						  <div class="col-md-6">
							<div class="form-group">
							  <input type="tel" class="form-control" placeholder="Phone">
							</div>
						  </div>
						  <div class="col-md-12">
							<div class="form-group">
							  <input type="text" class="form-control" placeholder="Subject">
							</div>
						  </div>
						  
						  <div class="col-lg-12">
						      <div class="form-group">
								<textarea name="message" rows="5" class="form-control" required="" placeholder="Message"></textarea>
							  </div>
						  </div>
						  <div class="col-lg-12">
							  <button name="submit" type="submit" value="Submit" class="btn btn-primary"> Send Message</button>
						  </div>
						</div>
					</form>
				</div>
				<div class="col-md-5 col-12 mt-30 mt-md-0">
					<div class="box box-body p-40 bg-dark mb-0">
						<h2 class="box-title text-white">Kontak</h2>
						<div class="widget fs-18 my-20 py-20 by-1 border-light">	
							<ul class="list list-unstyled text-white-80">
								<li class="ps-40"><i class="ti-location-pin"></i>Jl. Raya Rajapolah – Sindangkasih RT.09/RW.05 Kab. Ciamis<br>Jawa Barat</li>
								<li class="ps-40 my-20"><i class="ti-mobile"></i>(0265) 7521244</li>
								<li class="ps-40"><i class="ti-email"></i>smkkarnascms@gmail.com</li>
							</ul>
						</div>
						<h4 class="mb-20">Follow Us</h4>
						<ul class="list-unstyled d-flex gap-items-1">
							<li><a href="#" class="waves-effect waves-circle btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#" class="waves-effect waves-circle btn btn-social-icon btn-circle btn-twitter"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" class="waves-effect waves-circle btn btn-social-icon btn-circle btn-linkedin"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#" class="waves-effect waves-circle btn btn-social-icon btn-circle btn-youtube"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.488540573908!2d108.23102207500007!3d-7.2988785927087525!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6f50a170e708bf%3A0x7ec7b6480868116c!2sSMK%20KARYA%20NASIONAL%20SINDANGKASIH%20CIAMIS!5e0!3m2!1sid!2sid!4v1705044774912!5m2!1sid!2sid" class="map" style="border:0;" allowfullscreen="" ></iframe>
				<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15841.847596022304!2d108.4694458!3d-6.954714!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6f165e0b4b887b%3A0x35efb6884e01e723!2sSMK%20Karya%20Nasional!5e0!3m2!1sid!2sid!4v1702818633104!5m2!1sid!2sid" class="map" style="border:0;" allowfullscreen ></iframe> -->
				<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387193.30596552044!2d-74.25986763304465!3d40.69714941412697!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1537364999769" class="map" style="border:0" allowfullscreen></iframe> -->
				</div>
			</div>
		</div>
	</section>