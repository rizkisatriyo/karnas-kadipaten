        <!---page Title --->
        <section class="bg-img pt-120 " data-overlay="4" style="background-image: url(<?php echo base_url('assets') ?>/images/front-end-img/background/bg-9.png)">
			<div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">						
                            <h2 class="page-title text-white">Profil Sekolah</h2>
                            <ol class="breadcrumb bg-transparent justify-content-center">
                                <li class="breadcrumb-item"><a href="#" class="text-white-50"><i class="mdi mdi-home-outline"></i></a></li>
                                <li class="breadcrumb-item text-white active" aria-current="page">Profil Sekolah</li>
                            </ol>
                        </div>
                    </div>
                </div>
			</div>
		</section>

        <!--Page content -->
	
	<section class="py-50">
		<div class="container">
			<div class="row">
                <div class="col-xl-4 col-md-5 col-sm-12">					
					<div class="course-detail-bx">
						<div class="box box-body">
					        <img src="<?php echo base_url('assets') ?>/images/ciamis.JPG" alt=""/>
						</div>
                        
						<div class="box box-body">
							<div class="staff-bx">
                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-building"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Sekolah : <span class="fw-bold">SMK KARNAS SINDANGKASIH CIAMIS</span></h5>
									</div>
								</div>
							    <hr class="w-p100">
								<div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-user"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Kepsek : <span class="fw-bold">H. Edwan Gustiawan, S.T</span></h5>
									</div>
								</div>
							    <hr class="w-p100">

                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-star"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Akreditasi : <span class="fw-bold">A</span></h5>
									</div>
								</div>
							    <hr class="w-p100">

                                <!-- <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-user"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Operator : <span class="fw-bold">DENI MAULANA SIDIQ</span></h5>
									</div>
								</div>
							    <hr class="w-p100"> -->
                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-book"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Kurikulum : <span class="fw-bold">Kurikulum Merdeka</span></h5>
									</div>
								</div>
							</div>

						</div>
                        
						
					</div>
				</div>
				<div class="col-xl-8 col-md-7 col-12">
					<div class="box">
					  <div class="box-body">
						<h2 class="box-title mb-0 fw-500">Visi & Misi</h2>	

						<hr>
						<h4 class="box-title mb-0 fw-500">Visi</h4>	
						<p class="fs-16 mb-30">Menjadi lembaga pendidikan profesional yang menghasilkan lulusan<br>
						<i>” Smart – Competent – Religious “</i></p>	
						<h4 class="box-title mb-0 fw-500">Misi</h4>	
                        <ul class="list list-mark">
                            <li> Memberikan service exelent terhadap stakeholder dalam semua aspek sarana prasarana guna mewujudkan tenaga kerja yang produktif.</li>
                            <li> Meningkatkan mutu sumber daya manusia melalui dukungan IPTEK & IMTAQ.</li>
                            <li> Membekali bakal dasar kepada tamatan untuk pengembangan dirinya secara berkelanjutan.</li>
                            <li> Meningkatkan kualitas tamatan yang sesuai dengan standar kompetensi nasional dalam menghadapi era globalisasi.</li>
                        </ul>
                      </div>
					</div>
					<div class="box">
					  <div class="box-body">
						<h2 class="box-title mb-0 fw-500">Sejarah</h2>	
						<hr>
						<p class="fs-16 mb-30">
						SMK KarNas Sindangkasih didirikan pada tanggal 22 Januari 2013 dibawah naungan Yayasan Pendidkan Karya Nasional yang dipimpin oleh Bapak Drs. H. Dadang Djaka, BE. Smk Karnas sindangkasih pertama kali membuka dua jurusan yakni jurusan Rekayasa Perangkat lunak yang biasa disebut dengan RPL yang dimana jurusan itu menyangkut dengan teknologi IT dan yang kedua jurusan Teknik dan Bisnis Sepeda Motor yang biasa disebut dengan TBSm dimana jurusan itu menyangkut dengan berbagai macam kendaraan motor.
						</p>
						<p class="fs-16 mb-30">
						SMK KarNas Pertama kali beroperasi atau mulai menerima siswa-siswi pada tahun 2013 dimana pada saat itu hanya ada 26 orang siswa dan siswi diantaranya 16 siswa-siswi jurusan RPL dan 10 orang jurusan TBSm. Pada Saat itu SMK KarNas Sindangkasih baru memiliki 3 (tiga) lokal ruang pembelajaran yakni satu untuk bengkel produktif RPL, satu untuk bengkel produktif TBSM dan satu untuk ruang teori. adapun ruangan lain yaitu satu ruangan Kepala Sekolah, satu Ruangan Guru dan 1 ruangan TU (Tata Usaha).
						</p>
						<p class="fs-16 mb-30">
						Dari tahun ke tahun alhamdulillah SMK KarNas Sindangkasih semakin maju dengan bertambahnya siswa-siswi dan juga sarana prasarana, mulai dari peralatan bengkel TBSM, peralatan bengkel RPL,  ruangan teori, ruangan praktek dan lainnya.
						</p>
						<p class="fs-16 mb-30">
						SMK KarNas Sindangkasih merupakan sekolah yang berkomitment membekali siswa-siswinya dalam ilmu-ilmu sesuai dengan program studi, serta kemampuan dibidang teknologi, informasi, bahasa inggris, entrepreneur dan kepribadian yang unggul, serta terus berinovasi untuk menghasilkan lulusan yang memiliki kualifikasi tinggi dengan masa tunggu pendek yaitu segera memperoleh pekerjaan yang berkualitas ataupun berwiraswasta setelah lulus nanti.
						</p>
						<p class="fs-16 mb-30">
						Harapan lulusan SMK Karnas Sindangkasih Ciamis yaitu :
						
							Sudah memiliki keahlian.
							Santun, mampu dan mau bekerja keras.
							Sholeh, Mandiri, Kreatif.
							Sanggup melanjutkan kuliah.
						</p>
						
						<ul class="list list-mark">
                            <li> Sudah memiliki keahlian.</li>
                            <li> Santun, mampu dan mau bekerja keras.</li>
                            <li> Sholeh, Mandiri, Kreatif.</li>
                            <li> Sanggup melanjutkan kuliah.</li>
                        </ul>
					  </div>
					</div>
					<div>
						<ul class="course-overview list-unstyled b-1 bg-gray-100">
                            <li><i class="ti-calendar"></i> <span class="tag">NPSN </span> <span class="value">69892759</span></li>
                            <li><i class="fa fa-language"></i> <span class="tag">Bentuk Pendidikan </span> <span class="value">SMK</span></li>
                            <li><i class="ti-book"></i> <span class="tag">Status Kepemilikan </span> <span class="value">Yayasan</span></li>
                            <li><i class="ti-help-alt"></i> <span class="tag">SK Pendirian</span> <span class="value"> 421.3/876-Disdikbud/2015</span></li>
                            <li><i class="ti-time"></i> <span class="tag">Tanggal SK Pendirian</span> <span class="value">2015-03-18</span></li>
                            <li><i class="ti-stats-up"></i> <span class="tag">SK Izin Operasional</span> <span class="value">421.3/876-Disdikbud/2015</span></li>
                            <li><i class="ti-smallcap"></i> <span class="tag">Tanggal SK Izin Operasional </span> <span class="value">2015-03-18</span></li>
                        </ul>

					</div>
				</div>
								
			</div>
		</div>
	</section>