        <!---page Title --->
        <section class="bg-img pt-120 " data-overlay="4" style="background-image: url(<?php echo base_url('assets') ?>/images/front-end-img/background/bg-9.png)">
			<div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">						
                            <h2 class="page-title text-white">Teknik Sepeda Motor</h2>
                            <ol class="breadcrumb bg-transparent justify-content-center">
                                <li class="breadcrumb-item"><a href="#" class="text-white-50"><i class="mdi mdi-home-outline"></i></a></li>
                                <li class="breadcrumb-item text-white active" aria-current="page">Profil Sekolah</li>
                            </ol>
                        </div>
                    </div>
                </div>
			</div>
		</section>

        <!--Page content -->
	
	<section class="py-50">
		<div class="container">
			<div class="row">
                <div class="col-xl-4 col-md-5 col-sm-12">					
					<div class="course-detail-bx">
						<div class="box box-body">
					        <img src="<?php echo base_url('assets') ?>/images/ciamis.JPG" alt=""/>
						</div>
                        
						<div class="box box-body">
							<div class="staff-bx">
                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-building"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Sekolah : <span class="fw-bold">SMK KARNAS SINDANGKASIH CIAMIS</span></h5>
									</div>
								</div>
							    <hr class="w-p100">
								<div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-user"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Kepsek : <span class="fw-bold">H. Edwan Gustiawan, S.T</span></h5>
									</div>
								</div>
							    <hr class="w-p100">

                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-star"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Akreditasi : <span class="fw-bold">A</span></h5>
									</div>
								</div>
							    <hr class="w-p100">

                                <!-- <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-user"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Operator : <span class="fw-bold">DENI MAULANA SIDIQ</span></h5>
									</div>
								</div>
							    <hr class="w-p100"> -->
                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-book"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Kurikulum : <span class="fw-bold">Kurikulum Merdeka</span></h5>
									</div>
								</div>
							</div>

						</div>
                        
						
					</div>
				</div>
				<div class="col-xl-8 col-md-7 col-12">
					<div class="box">
					  <div class="box-body">
						<h2 class="box-title mb-0 fw-500">Teknik Sepeda Motor</h2>	

						<hr>
			                <p><strong><img style="display: block; margin-left: auto; margin-right: auto;" src="../assets/images/tsm.jpg" alt="" width="100%" ></strong></p>
						
                      </div>
					</div>
					<div class="box">
					  <div class="box-body">
                      <div class="caption">
                            <h3>TEKNIK SEPEDA MOTOR</h3>
                            <p><strong>VISI PROGRAM KEAHLIAN</strong></p>
                            <p>Menjadikan Teknik Sepeda Motor sebagai program keahlian yang unggul, profesional dan berakhlaqul karimah.</p>
                            <p><strong>MISI PROGRAM KEAHLIAN</strong></p>
                            <ul>
                            <li>Mengembangkan pembelajaran dengan orientasi life skill</li>
                            <li>Mengedepankan kemampuan intelektual</li>
                            <li>Berwawasan global</li>
                            <li>Berintegrasi sosial dan berorientasi pasar</li>
                            <li>Berakar budaya lokal mencapai Akhlaqul Karimah</li>
                            </ul>
                            <p><strong>TUJUAN PROGRAM KEAHLIAN</strong></p>
                            <ul>
                            <li>Mempersiapkan peserta didik menjadi manusia islami yang produktif, mampu bekerja mandiri dan dapat diserap oleh DU/DI sebagai tenaga kerja tingkat menengah sesuai dengan kompotensi yang dimilikinya.</li>
                            <li>Memberikan pembekalan agar mampu berkarir, ulet dan giat dalam berkompetensi, mampu beradaptasi dilingkungan kerja dan dapat mengembangkan sikap propesional sesuai kompetensi yang dimilikinya.</li>
                            <li>Membekali peserta didik dalam ilmu pengetahuan, teknologi, seni dan wawasan entreupreneur agar mampu mengembangkan diri dikemudian hari baik secara mandiri maupun melanjutkan pada jenjang pendidikan lebih tinggi.</li>
                            <li>Membekali peserta didik dengan keterampilan, pengetahuan dan sikap agar kompeten di bidang :</li>
                            <li>Perawatan dan perbaikan motor (engine) / tune up.</li>
                            <li>Perawatan dan perbaikan sistem pemindah tenaga/ transmisi.</li>
                            <li>Perawatan dan perbaikan chasis dan suspensi.</li>
                            <li>Perawatan dan perbaikan sistem kelistrikan.</li>
                            </ul>
                            <p><strong>KOMPETENSI KEAHLIAN YANG DI AJARKAN</strong></p>
                            <p>Kompetensi yang diajarkan di Kompetensi Keahlian Teknik Sepeda Motor di SMK Karya Nasional Sindangkasih meliputi :</p>
                            <p><strong>DASAR KOMPETENSI KEJURUAN</strong></p>
                            <ul>
                            <li>Memahami dasar-dasar mesin</li>
                            <li>Memahami proses-proses dasar pembentukan logam</li>
                            <li>Menjelaskan proses-proses mesin konversi energi</li>
                            <li>Menginterpretasikan gambar teknik</li>
                            <li>Menggunakan peralatan dan perlengkapan di tempat kerja</li>
                            <li>Menggunakan alat-alat ukur (<em>measuring tools</em>)</li>
                            <li>Menerapkan prosedur keselamatan, kesehatan kerja dan lingkungan tempat kerja</li>
                            </ul>
                            <p><strong>KOMPETENSI KEJURUAN</strong></p>
                            <ul>
                            <li>Melakukan perbaikan sistem hidrolik sepeda motor</li>
                            <li>Memperbaiki sistem gas buang</li>
                            <li>Memelihara baterai</li>
                            <li>Melaksanakan&nbsp;<em>overhaul&nbsp;</em>kepala silinder</li>
                            <li>Melakukan&nbsp;<em>overhaul</em>&nbsp;sistem pendingin berikut komponen-komponennya</li>
                            <li>Melakukan perbaikan sistem bahan bakar sepeda motor</li>
                            <li>Melakukan perbaikan&nbsp;<em>engine</em>&nbsp;sepeda motor berikut komponen-komponennya</li>
                            <li>Melakukan perbaikan unit kopling sepeda motor berikut komponen-komponen sistem pengoperasiannya</li>
                            <li>Melakukan perbaikan sistem transmisi manual</li>
                            <li>Melakukan perbaikan sistem transmisi otomatis</li>
                            <li>Melakukan perbaikan sistem rem</li>
                            <li>Melakukan perbaikan sistem suspensi</li>
                            <li>Melaksanakan pekerjaan servis pada roda, ban, dan rantai</li>
                            <li>Melakukan perbaikan ringan pada rangkaian sistem kelistrikan dan instrumen</li>
                            <li>Melakukan perbaikan sistem starter</li>
                            <li>Melakukan perbaikan sistem pengisian</li>
                            <li>Melakukan perbaikan sistem pengapian</li>
                            </ul>
                            <p><strong>KOMPETENSI TAMATAN</strong></p>
                            <p>Kemampuan tamatan Program Keahlian Teknik Sepeda Motor adalah :</p>
                            <ul>
                            <li>Mampu memberikan jasa pelayanan pemeliharaan dan perbaikan di bidang perbengkelan sepeda Motor.</li>
                            <li>Mampu mengidentifikasi jenis kerusakan dari seluru sistem otomotif sepeda motor beserta komponen-komponennya.</li>
                            <li>Mampu membongkar, memperbaiki dan mengganti seluruh sistem otomotif sepeda motor berikut komponennya .</li>
                            </ul>
                            <p><strong>LINGKUP PEKERJAAN</strong></p>
                            <p>Bidang pekerjaan yang dapat diisi oleh tamatan Kompetensi Keahlian Teknik Sepeda Motor antara lain :</p>
                            <ul>
                            <li>Teknisi atau Mekanik pada Bengkel Sepeda Motor</li>
                            <li>Teknisi perakitan atau teknisi produksi pada industri Sepeda Motor.</li>
                            <li>Wirausahawan dalam bidang Otomotif (Bengkel Sepeda Motor).</li>
                            </ul>			
					  </div>
					</div>
				</div>
			</div>
		</div>
	</section>  