    <!---page Title --->
	<section class="bg-img pt-120 " data-overlay="4" style="background-image: url(<?php echo base_url('assets') ?>/images/front-end-img/background/bg-9.png)">
			<div class="container">
				<div class="row">
					<div class="col-12">    
						<div class="text-center">						
                            <h2 class="page-title text-white">Berita</h2>
                            <ol class="breadcrumb bg-transparent justify-content-center">
                                <li class="breadcrumb-item"><a href="#" class="text-white-50"><i class="mdi mdi-home-outline"></i></a></li>
                                <li class="breadcrumb-item text-white active" aria-current="page">Berita dan Artikel</li>
                            </ol>
                        </div>
					</div>
				</div>
			</div>
		</section>
	<!--Page content -->
	
	<section class="py-50">
		<div class="container">
			<div class="row">	
				<div class="col-lg-9 col-md-8 col-12">
					<div class="row">    
						<?php 
						if (is_array($data) || is_object($data))
						{
							foreach ($data as $item) { ?>
						<div class="col-md-6 col-12">
							<div class="blog-post">
								<div class="entry-image clearfix">
									<img class="img-fluid" src="<?php echo base_url_assets(); ?>news/<?= $item['f_photo'] ?>" alt="">
								</div>
								<div class="blog-detail">
									<div class="entry-title mb-10">
										<a href="<?php base_url()?>blogDetail/?id=<?= $item['_id']?>"><?=$item['title'] ?></a>
									</div>
									<div class="entry-meta mb-10">
										<ul class="list-unstyled">
											<li><a href="#"><i class="fa fa-user-o"></i> <?= $item['f_created_by'] ?></a></li>
											<!-- <li><a href="#"><i class="fa fa-comment-o"></i> 5</a></li> -->
											<li><a href="#"><i class="fa fa-calendar-o"></i> <?= $item['f_created_on'] ?></a></li>
										</ul>
									</div>
									<div class="entry-content">
										<?php echo substr($item['description'], 0, 80); ?>
									</div>
									<div class="entry-share d-flex justify-content-between align-items-center">
										<div class="entry-button">
											<a href="<?php base_url()?>blogDetail/?id=<?= $item['_id']?>" class="btn btn-primary btn-sm">Read more</a>
										</div>
										<div class="social">
											<strong>Share : </strong>
											<ul class="list-unstyled">
												<li>
													<a href="#"> <i class="fa fa-facebook"></i> </a>
												</li>
												<li>
													<a href="#"> <i class="fa fa-twitter"></i> </a>
												</li>
												<li>
													<a href="#"> <i class="fa fa-pinterest-p"></i> </a>
												</li>
												<li>
													<a href="#"> <i class="fa fa-dribbble"></i> </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } } ?>      

					</div>
				</div>			
				<div class="col-lg-3 col-md-4 col-12">
					<div class="side-block px-20 py-10 bg-white">
						<div class="widget courses-search-bx placeholdertx mb-10">
							<div class="form-group">
								<div class="input-group">
									<label class="form-label">Search...</label>
									<input name="name" type="text" required="" class="form-control">
								</div>
							</div>
						</div>	
						<div class="widget clearfix">
							<h4 class="pb-15 mb-25 bb-1">Recent Posts </h4>
							<?php 
							if (is_array($data) || is_object($data))
							{
							foreach ($data as $item) { ?>
							<div class="recent-post clearfix">
								<div class="recent-post-image">
									<img class="img-fluid bg-primary-light" src="<?php echo base_url_assets(); ?>news/<?= $item['f_photo'] ?>" alt="">
								</div>
								<div class="recent-post-info">
									<a href="<?php base_url()?>berita/detail/?id=<?= $item['_id']?>"><?= $item['title'] ?></a>
									<span><i class="fa fa-calendar-o"></i> <?= $item['f_created_on'] ?></span>
								</div>
							</div>
							<?php } 
							} ?>
						</div>
						<div class="widget clearfix">
							<h4 class="pb-15 mb-15 bb-1">Categories</h4>
							<div class="media-list media-list-divided">
								<a class="px-0 media media-single" href="#">
								  <span class="title ms-0">Biology Course </span>
								  <span class="mx-0 badge badge-secondary-light">125</span>
								</a>

								<a class="px-0 media media-single" href="#">
								  <span class="title ms-0">Contemporary Art</span>
								  <span class="mx-0 badge badge-primary-light">124</span>
								</a>

								<a class="px-0 media media-single" href="#">
								  <span class="title ms-0">Elizabethan Theater</span>
								  <span class="mx-0 badge badge-info-light">425</span>
								</a>

								<a class="px-0 media media-single" href="#">
								  <span class="title ms-0">Geometry Course</span>
								  <span class="mx-0 badge badge-success-light">321</span>
								</a>

								<a class="px-0 media media-single" href="#">
								  <span class="title ms-0">Informatic Course</span>
								  <span class="mx-0 badge badge-danger-light">159</span>
								</a>

								<a class="px-0 media media-single" href="#">
								  <span class="title ms-0">Live Drawing</span>
								  <span class="mx-0 badge badge-warning-light">452</span>
								</a>
							  </div>
						</div>
						<div class="widget clearfix">
							<h4 class="pb-15 mb-25 bb-1">Archives</h4>
							<ul class="list list-unstyled">
								<li><a href="#"><i class="fa fa-angle-double-right"></i> November 2020</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i> October 2020</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i> September 2020</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i> August 2020</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i> July 2020</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</section>	
	
	