<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative"> 
        <div class="multinav">
          <div class="multinav-scroll" style="height: 100%;">   
              <!-- sidebar menu-->

              <ul class="sidebar-menu" data-widget="tree">  
              <?php if ($this->session->userdata('id_role') == "1") { ?>
              
                <li class="header"></li>
                <li class="treeview">
                  <a href="<?php echo base_url();?>admin/home">
                    <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
                    <span>Dashboard</span>
                   
                  </a>
                </li>

                <li class="header">E-Proposal </li>
                <li class="treeview">
                  <a href="#">
                    <i class="icon-Write"><span class="path1"></span><span class="path2"></span></i>
                    <span>Proposal</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-right pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">            
                    <li><a href="<?php echo base_url();?>admin/proposal"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Pengajuan Proposal</a></li>                                                                                
                  </ul>
                </li>           
                <li class="treeview">
                  <a href="#">
                    <i class="icon-File"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                    <span>Report</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-right pull-right"></i>
                    </span>
                  </a>                                    
                </li>   
                 <li class="header">SISAYANG </li>
                <li class="treeview">
                  <a href="#">
                    <i class="icon-Write"><span class="path1"></span><span class="path2"></span></i>
                    <span>Vaksinasi</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-right pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">            
                    <li><a href="<?php echo base_url();?>admin/proposal"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Data Hewan</a></li>                                                                                
                  </ul>
                </li>           
                <li class="treeview">
                  <a href="#">
                    <i class="icon-File"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                    <span>Report</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-right pull-right"></i>
                    </span>
                  </a>                                    
                </li>              
                <li class="header">COLLECTIONS</li>
                <li class="treeview">
                  <a href="#">
                    <i class="icon-Library"><span class="path1"></span><span class="path2"></span></i>
                    <span>Website</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-right pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="widgets_blog.html"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Blog</a></li>
                                     
                    <li class="treeview">
                        <a href="#">
                            <i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Maps
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="map_google.html"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Google Map</a></li>
                            <li><a href="map_vector.html"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Vector Map</a></li>
                        </ul>
                    </li>                       
                    <li class="treeview">
                        <a href="#">
                            <i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Modals
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="component_modals.html"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Modals</a></li>
                            <li><a href="component_sweatalert.html"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Sweet Alert</a></li>
                            <li><a href="component_notification.html"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Toastr</a></li>
                        </ul>
                    </li>
                  </ul>
                </li>               
                <?php } 
                if ($this->session->userdata('id_role') == "2") { ?>
              
                <li class="header"></li>
                <li>
                  <a href="<?php echo base_url();?>ponakan/home">
                    <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
                    <span>Dashboard</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url();?>ponakan/pengajuan/list">
                    <i class="icon-Write"><span class="path1"></span><span class="path2"></span></i>
                    <span>Proposal</span>
                  </a>
                  
                </li>           
                                  
      
                 <?php } 
                if ($this->session->userdata('id_role') == "3") {
                 ?>
              
                <li class="header"></li>
                <li class="treeview">
                  <a href="<?php echo base_url();?>admin/home">
                    <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
                    <span>Dashboard</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                </li>

                <li class="treeview">
                  <a href="#">
                    <i class="icon-Write"><span class="path1"></span><span class="path2"></span></i>
                    <span>Data Vaksinasi</span>
                    <span class="pull-right-container">
                    </span>
                  </a>
                </li>               

                 <?php } ?>
                 
              </ul>
          </div>
        </div>
    </section>
    <div class="sidebar-footer">
        <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings" aria-describedby="tooltip92529"><span class="icon-Settings-2"></span></a>
        <a href="mailbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><span class="icon-Mail"></span></a>
        <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><span class="icon-Lock-overturning"><span class="path1"></span><span class="path2"></span></span></a>
    </div>
  </aside>