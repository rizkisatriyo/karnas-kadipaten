	<!-- Vendors Style-->
	<link rel="stylesheet" href="<?php echo base_url('assets/front-end');?>/css/vendors_css.css">
		
	  <!-- Style-->  
	  <link rel="stylesheet" href="<?php echo base_url('assets/front-end');?>/css/style.css">
	  <link rel="stylesheet" href="<?php echo base_url('assets/front-end');?>/css/skin_color.css">


		<style>
			.footer {
				left: 0;
				bottom: 0;
				width: 100%;
				color: white;
				text-align: center;
				background-color: <?=$color_dominan ?>;
				z-index: 0;
			}

			.floatwa {
				position: fixed;
				width: 60px;
				height: 60px;
				bottom: 40px;
				right: 40px;
				background-color: #25d366;
				color: #FFF;
				border-radius: 50px;
				text-align: center;
				font-size: 30px;
				box-shadow: 5px 5px 29px #002905;
				z-index: 100;
			}

			.my-float-wa {
				margin-top: 16px;
			}
		</style>