    <ul class="menu">	
				<li>
					<a href="<?php echo base_url() ?>" class="fw-bold">Home</a>
				</li>				
				<li>
					<a href="<?php echo base_url('website/profil') ?>" class="fw-bold">Profil</a>
				</li>				

				<li class="megamenu">
					<a href="#" class="fw-bold">Akademik</a>
					<div class="megamenu-content">
						<div class="row">
							
							<div class="col-lg-3 col-12">
								<ul class="list-group">
									<li><h4 class="menu-title">Kompetensi Keahlian</h4></li>
									<li><a href="<?php echo base_url('website/rpl') ?>"><i class="ti-arrow-circle-right me-10"></i>Rekayasa Perangkat Lunak</a></li>
									<li><a href="<?php echo base_url('website/tsm') ?>"><i class="ti-arrow-circle-right me-10"></i>Teknik Bisnis Sepeda Motor</a></li>
									<li><a href="#"><i class="ti-arrow-circle-right me-10"></i>Teknik Ototronik</a></li>
								</ul>
							</div>
							<div class="col-lg-3 col-12">
								<ul class="list-group">
									<li><h4 class="menu-title">Kurikulum</h4></li>
									<!-- <li><a href="faqs.html"><i class="ti-arrow-circle-right me-10"></i>Pengumuman</a></li>
									<li><a href="inovice.html"><i class="ti-arrow-circle-right me-10"></i>Kurtilas</a></li>
									<li><a href="inovice.html"><i class="ti-arrow-circle-right me-10"></i>Kurikulum Merdeka</a></li>
									<li><a href="inovice.html"><i class="ti-arrow-circle-right me-10"></i>Guru Pengajar</a></li> -->
									
								</ul>
							</div>
							<div class="col-lg-3 col-12">
								<ul class="list-group">
									<li><h4 class="menu-title">Ektrakulikuler</h4></li>
									<!-- <li><a href="header_default.html"><i class="ti-arrow-circle-right me-10"></i>Sepak Bola</a></li>
									<li><a href="header_style2.html"><i class="ti-arrow-circle-right me-10"></i>Musik</a></li>
									<li><a href="header_style3.html"><i class="ti-arrow-circle-right me-10"></i>Voly</a></li>
									<li><a href="header_style4.html"><i class="ti-arrow-circle-right me-10"></i>Marawis</a></li> -->
								</ul>
							</div>
							<div class="col-lg-3 col-12">
								<ul class="list-group">
									<li><h4 class="menu-title">OSIS</h4></li>
									<!-- <li><a href="header_default.html"><i class="ti-arrow-circle-right me-10"></i>Sepak Bola</a></li>
									<li><a href="header_style2.html"><i class="ti-arrow-circle-right me-10"></i>Musik</a></li>
									<li><a href="header_style3.html"><i class="ti-arrow-circle-right me-10"></i>Voly</a></li>
									<li><a href="header_style4.html"><i class="ti-arrow-circle-right me-10"></i>Marawis</a></li> -->
								</ul>
							</div>
							
						</div>
					</div>
				</li>				
				<li class="dropdown">
					<a href="#" class="fw-bold">Blog</a>
					<ul class="dropdown-menu">
						<li>
							<a href="<?php echo base_url('website/blog') ?>">Berita</a>
						</li>
						<li>
							<a href="<?php echo base_url('website/blog') ?>">Artikel</a>
						</li>
						<li class="dropdown">
							<a href="#">Galeri</a>
							<ul class="dropdown-menu">
								<li><a href="blog_single_grid_post.html">Foto</a></li>
								<li><a href="blog_single_html5video_post.html">Video</a></li>
							</ul>
						</li>
					</ul>
				</li>		

				<li>
					<a href="<?php echo base_url('website/contactUs') ?>" class="fw-bold">Hubungi Kami</a>
				</li>
			</ul>