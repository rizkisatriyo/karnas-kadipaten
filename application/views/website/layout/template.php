<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('assets');?>/images/icon.jpeg">

    <title>Karnas - Smart - Competence - Religious</title>
    
    <?php include '_css.php';?>
	
     
  </head>

<body class="theme-primary">
  <div id="overlay" onclick="off()">
  </div>
    <?php include '_header.php';?>

	
	
	<!---page Title --->
	<section data-overlay="4" >
		<div class="container">
			<div class="row">
				<div class="col-12">    
					<div class="text-center">						
						
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- slider -->

    <?php echo $contents ;?>

    <?php include '_footer.php';?>
    <a href="https://wa.me/6289646544043?text=Halo admin" class="floatwa" target="_blank">
        <i class="fab fa-whatsapp my-float-wa"></i>
    </a>


  <!-- iklan pop up -->
<!-- modal Area -->              
  <!-- <div class="modal fade" id="myModal" style="z-index:999999999">
	  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          <p>One fine body&hellip;</p>
          </div>

      </div>
	  </div>
  </div> -->
  <!-- /.modal -->


    <?php include '_js.php';?>
	
</body>
</html>

