	<header class="top-bar">
		<div class="topbar">

		  <div class="container">
			<div class="row justify-content-end">
			  <div class="col-lg-6 col-12 d-lg-block d-none">
				<div class="topbar-social text-center text-md-start topbar-left">
				  <ul class="list-inline d-md-flex d-inline-block">
					<li class="ms-10 pe-10"><a href="<?php echo base_url('website/faqs')?>"><i class="text-white fa fa-question-circle"></i> Faq</a></li>
					<li class="ms-10 pe-10"><a href="#"><i class="text-white fa fa-envelope"></i> smkkarnascms@gmail.com</a></li>
					<li class="ms-10 pe-10"><a href="#"><i class="text-white fa fa-phone"></i> (0265) 7521244</a></li>
				  </ul>
				</div>
			  </div>
			  <div class="col-lg-6 col-12 xs-mb-10">
				<div class="topbar-call text-center text-lg-end topbar-right ">
				  <ul class="list-inline d-lg-flex justify-content-end ">				  
					 <!-- <li class="me-10 ps-10"><a href="#"><i class="text-white fa fa-user d-md-inline-block d-none"></i> Register</a></li>
					 <li class="me-10 ps-10"><a href="#"><i class="text-white fa fa-sign-in d-md-inline-block d-none"></i> Login</a></li> -->
					 <!-- <li class="me-10 ps-10"><a href="#"><i class="text-white fa fa-dashboard d-md-inline-block d-none"></i> My Account</a></li> -->
				  </ul>
				</div>
			  </div>
			 </div>
		  </div>
		</div>

		<nav hidden class="nav-white nav-transparent">
			<div class="nav-header">
				<a href="<?php echo base_url() ?>" class="brand">
					<img src="<?php echo base_url('assets') ?>/images/ciamis.jpg" alt="SMK KARNAS SINDANGKASIH CIAMIS" />
				</a>
				<button class="toggle-bar">
					<span class="ti-menu"></span>
				</button>	
			</div>								
			<!-- menu -->
			<?php include '_nav.php'; ?>

			<!-- <ul class="attributes">
				<li class="d-md-block d-none"><a href="<?php echo base_url('ppdb/register')?>" class="px-10 pt-15 pb-10"><div class="btn btn-primary py-5">PPDB 2024</div></a></li>
				<li><a href="#" class="toggle-search-fullscreen"><span class="ti-search"></span></a></li>

			</ul> -->

			<div class="wrap-search-fullscreen">
				<div class="container">
					<button class="close-search"><span class="ti-close"></span></button>
					<input type="text" placeholder="Search..." />
				</div>
			</div>
		</nav>
	</header>