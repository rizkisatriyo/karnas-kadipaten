	<footer class="footer_three">
		<div class="footer-top bg-dark3 pt-50">
            <div class="container">
                <div class="row">
					<div class="col-lg-9 col-12">
                        <div class="widget">
                            <h4 class="footer-title">About</h4>
							<hr class="bg-primary mb-10 mt-0 d-inline-block mx-auto w-60">
							<p class="text-capitalize mb-20">
                                    SMK KarNas Sindangkasih merupakan sekolah yang berkomitment membekali siswa-siswinya dalam ilmu-ilmu sesuai dengan program studi, serta kemampuan dibidang teknologi, informasi, bahasa inggris, entrepreneur dan kepribadian yang unggul, serta terus berinovasi untuk menghasilkan lulusan yang memiliki kualifikasi tinggi dengan masa tunggu pendek yaitu segera memperoleh pekerjaan yang berkualitas ataupun berwiraswasta setelah lulus nanti. 
                        </div>
                    </div>	
                    <div class="col-lg-3 col-12">
                        <div class="widget">

                            <h4 class="footer-title">Download Aplikasi Siap School</h4>
                            <hr class="bg-primary mb-10 mt-0 d-inline-block mx-auto w-60">
                            <br>
                            <a href="https://play.google.com/store/apps/details?id=siapschool.os.webview&hl=id&gl=US">
                                <img src="<?php echo base_url('assets/');?>images/download.png" width="170px" alt="">
                            </a>
                        </div>
                    </div>										
					
                </div>				
            </div>
        </div>
		
		<div class="footer-bottom bg-dark3">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 col-12 text-md-start text-center"> © 2023 <span class="text-white">SMK KARNAS SINDANGKASIH CIAMIS</span>  All Rights Reserved.</div>
					<div class="col-md-6 mt-md-0 mt-20">
						<div class="text-end">
                            <span class="text-white">Powered By <a href="https://siapschool.com" class="text-white">Siap School</a> .</span>
						</div>
					</div>
                </div>
            </div>
        </div>
	</footer>