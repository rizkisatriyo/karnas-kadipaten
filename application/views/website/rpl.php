        <!---page Title --->
        <section class="bg-img pt-120 " data-overlay="4" style="background-image: url(<?php echo base_url('assets') ?>/images/front-end-img/background/bg-9.png)">
			<div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">						
                            <h2 class="page-title text-white">Rekayasa perangkat Lunak</h2>
                            <ol class="breadcrumb bg-transparent justify-content-center">
                                <li class="breadcrumb-item"><a href="#" class="text-white-50"><i class="mdi mdi-home-outline"></i></a></li>
                                <li class="breadcrumb-item text-white active" aria-current="page">Profil Sekolah</li>
                            </ol>
                        </div>
                    </div>
                </div>
			</div>
		</section>

        <!--Page content -->
	
	<section class="py-50">
		<div class="container">
			<div class="row">
                <div class="col-xl-4 col-md-5 col-sm-12">					
					<div class="course-detail-bx">
						<div class="box box-body">
					        <img src="<?php echo base_url('assets') ?>/images/ciamis.JPG" alt=""/>
						</div>
                        
						<div class="box box-body">
							<div class="staff-bx">
                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-building"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Sekolah : <span class="fw-bold">SMK KARNAS SINDANGKASIH CIAMIS</span></h5>
									</div>
								</div>
							    <hr class="w-p100">
								<div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-user"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Kepsek : <span class="fw-bold">H. Edwan Gustiawan, S.T</span></h5>
									</div>
								</div>
							    <hr class="w-p100">

                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-star"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Akreditasi : <span class="fw-bold">A</span></h5>
									</div>
								</div>
							    <hr class="w-p100">

                                <!-- <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-user"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Operator : <span class="fw-bold">DENI MAULANA SIDIQ</span></h5>
									</div>
								</div>
							    <hr class="w-p100"> -->
                                <div class="staff-info d-flex align-items-center">
                                    <div class=" rounded h-30 w-30 l-h-30 text-center me-10">
											<i class="fa fa-book"></i>
										</div>
									<div class="staff-name">
										<h5 class="mb-0">Kurikulum : <span class="fw-bold">Kurikulum Merdeka</span></h5>
									</div>
								</div>
							</div>

						</div>
                        
						
					</div>
				</div>
				<div class="col-xl-8 col-md-7 col-12">
					<div class="box">
					  <div class="box-body">
						<h2 class="box-title mb-0 fw-500">Rekayasa Perangkat Lunak</h2>	

						<hr>
			                <p><strong><img style="display: block; margin-left: auto; margin-right: auto;" src="../assets/images/rpl.jpg" alt="" width="100%" ></strong></p>
						
                      </div>
					</div>
					<div class="box">
					  <div class="box-body">
                      <div class="caption">
                            <p><strong>VISI PROGRAM KEAHLIAN</strong></p>
                            <p>Menjadikan Program Keahlian Rekayasa Perangkat Lunak (RPL) sebagai program keahlian yang terpercaya, mengutamakan profesionalisme, kualitas dan unggul serta berakhlaqul karimah.</p>
                            <p><strong>MISI PROGRAM KEAHLIAN</strong></p>
                            <ul>
                            <li>Menyiapkan tenaga yang terampil di bidang Rekayasa Perangkat Lunak</li>
                            <li>Membentuk tamatan yang berkepribadian unggul dan mampu mengembangkan diri</li>
                            <li>Menyiapkan wirausaha yang handal di bidang Rekayasa Perangkat Lunak</li>
                            </ul>
                            <p><strong>TUJUAN PROGRAM KEAHLIAN TEKNIK KOMPUTER DAN JARINGAN</strong></p>
                            <p>Program Keahlian Rekayasa Perangkat Lunak pada SMK Karya Nasional Sindangkasih bertujuan untuk :</p>
                            <ul>
                            <li>Meningkatkan keimanan dan ketaqwaan peserta didik</li>
                            <li>Mendidik peserta didik agar menjadi warga negara yang bertanggung jawab</li>
                            <li>Mendidik peserta didik agar dapat menerapkan hidup sehat, memiliki wawasan pengetahuan dan seni</li>
                            <li>Mendidik peserta didik dengan keahlian dan ketrampilan dalam progran keahlian Rekayasa Perangkat Lunak, agar dapat bekerja baik secara mandiri atau mengisi pekerjaan yang ada di DUDI sebagai tenaga kerja tingkat menengah</li>
                            <li>Mendidik Peserta didik agar mampu memilih karir, berkompetisi dan mengembangkan sikap professional dalam program keahlian Rekayasa Perangkat Lunak.</li>
                            <li>Membekali peserta didik dengan ilmu pengetahuan dan keterampilan sebagai bekal bagi yang berminat untuk melanjutkan pendidikan yang lebih tinggi.</li>
                            </ul>
                            <p><strong>KOMPETENSI KEAHLIAN YANG DI AJARKAN</strong></p>
                            <p>Kompetensi yang diajarkan pada Kompetensi Keahlian Rekayasa Perangkat Lunak di SMK Karya Nasional Sindangkasih meliputi:</p>
                            <p><strong>DASAR KOMPETENSI KEJURUAN</strong></p>
                            <ul>
                            <li>Merakit personal computer</li>
                            <li>Melakukan instalasi sistem operasi dasar</li>
                            <li>Menerapkan Keselamatan, Kesehatan Kerja dan Lingkungan Hidup (K3LH)</li>
                            <li>Membuat program aplikasi berbasis desktop</li>
                            <li>Membuat program aplikasi berbasis web</li>
                            <li>Membuat program aplikasi berbasis android</li>
                            </ul>
                            <p><strong>KOMPETENSI KEJURUAN</strong></p>
                            <ul>
                            <li>
                            <p>Menerapkan teknik elektronika analog dan digital dasar</p>
                            </li>
                            <li>
                            <p>Menerapkan algoritma pemrograman tingkat dasar</p>
                            </li>
                            <li>
                            <p>Menerapkan algoritma pemrograman tingkat lanjut</p>
                            </li>
                            <li>
                            <p>Membuat basis data</p>
                            </li>
                            <li>
                            <p>Menerapkan aplikasi basis data</p>
                            </li>
                            <li>
                            <p>Memahami pemrograman visual berbasis desktop</p>
                            </li>
                            <li>
                            <p>Membuat paket software aplikasi berbasis desktop</p>
                            </li>
                            <li>
                            <p>Mengoperasikan sistem operasi jaringan komputer</p>
                            </li>
                            <li>
                            <p>Menerapkan bahasa pemrograman SQL tingkat dasar</p>
                            </li>
                            <li>
                            <p>Menerapkan bahasa pemrograman SQL tingkat lanjut</p>
                            </li>
                            <li>
                            <p>Menerapkan dasar-dasar pembuatan web statis tingkat dasar</p>
                            </li>
                            <li>
                            <p>Membuat halaman web dinamis tingkat dasar</p>
                            </li>
                            <li>
                            <p>Membuat halaman web dinamis tingkat lanjut</p>
                            </li>
                            <li>
                            <p>Merancang aplikasi teks dan desktop berbasis objek</p>
                            </li>
                            <li>
                            <p>Menggunakan bahasa pemrograman berorientasi objek</p>
                            </li>
                            <li>
                            <p>Merancang program aplikasi web berbasis objek</p>
                            </li>
                            <li>
                            <p>Membuat aplikasi basis data menggunakan SQL</p>
                            </li>
                            <li>
                            <p>Mengintegrasikan basis data dengan sebuah web</p>
                            </li>
                            <li>
                            <p>Membuat program basis data</p>
                            </li>
                            <li>
                            <p>Membuat aplikasi web berbasis JSP</p>
                            </li>
                            </ul>
                            <p><strong>KOMPETENSI TAMATAN</strong></p>
                            <p>Tamatan program keahlian Teknik Komputer dan Jaringan dapat menampilkan diri sebagai manusia beriman dan bertaqwa kepada Tuhan Yang Maha Esa, berbudi pekerti luhur, sehat jasmani dan rohani, berkepribadian yang mantap dan mandiri serta mempunyai tanggung jawab kemasyarakatan dan kebangsaan. Kompetensi produktif yang dimiliki tamatan program keahlian Teknik Komputer dan Jaringan adalah seperti tercantum pada profil kompetensi tamatan berikut :</p>
                            <ul>
                            <li>Mampu melakukan komunikasi ditempat kerja</li>
                            <li>Dapat melaksanakan persyaratan keselamatan kesehatan kerja (K3) sesuai dengan peraturan dan standar yang ada</li>
                            <li>Mampu merakit, memperbaiki dan merawat komputer personal.</li>
                            <li>Mampu menginstalasi perangkat komputer personal dan menginstal sistem operasi dan aplikasi.</li>
                            <li>Mampu menginstalasi perangkat jaringan berbasis lokal.</li>
                            <li>Mampu menginstalasi perangkat jaringan berbasis luas.</li>
                            <li>Merancang bangun dan mengadministrasi jaringan berbasis luas.</li>
                            <li>Merancang bangun dan menganalisa WAN</li>
                            </ul>
                            <p><strong>LINGKUP PEKERJAAN</strong></p>
                            <p>Bidang pekerjaan yang dapat diisi oleh tamatan Kompetensi Keahlian Teknik Komputer Dan Jaringan antara lain :</p>
                            <ol>
                            <li>Internet Service Provider Perusahaan Jaringan Network Security<br> Lingkup Kerja :
                            <ul>
                            <li>Mengidentifikasi kebutuhan keamanan jaringan</li>
                            <li>Mendesain sistem keamanan jaringan</li>
                            <li>Menginstalasi security jaringan</li>
                            <li>Menginstalasi dan administrasi server authentikasi</li>
                            <li>Mengoperasikan security jaringan</li>
                            <li>Monitoring keamanan jaringan</li>
                            </ul>
                            </li>
                            <li>Wireless Networking<br> Lingkup Kerja :
                            <ul>
                            <li>Merancang, melakukan survey lapangan</li>
                            <li>Membuat antenna</li>
                            <li>Menginstalasi jaringan wireless</li>
                            <li>Mengkonfigurasi peralatan</li>
                            <li>Mengoperasikan jaringan wireless</li>
                            </ul>
                            </li>
                            <li>Network Administrator<br> Lingkup Kerja :
                            <ul>
                            <li>Mendiagnosis permasalahan perangkat yang tersambung jaringan berbasis luas (Wide Area Network)</li>
                            <li>Menganalisa dan memperbaiki kerusakan/kesalahan /tidak bekerjanya koneksi di sistem jaringan</li>
                            </ul>
                            </li>
                            <li>Administrasi Server<br> Lingkup Kerja :
                            <ul>
                            <li>Memanage server</li>
                            <li>Mengatur band width</li>
                            <li>File sharing</li>
                            <li>Memonitor server</li>
                            <li>Mengatur traffic</li>
                            </ul>
                            </li>
                            <li>Computer Integrator<br> Lingkup Kerja :
                            <ul>
                            <li>Merakit komputer</li>
                            <li>Memperbaiki komputer</li>
                            <li>Menginstalasi sistem operasi GUI dan TEXT</li>
                            </ul>
                            </li>
                            <li>Voip Integrator<br> Lingkup Kerja :
                            <ul>
                            <li>Indentifikasi kebutuhan</li>
                            <li>Merancang jaringan voip</li>
                            <li>Instalasi softswitch</li>
                            </ul>
                            </li>
                            <li>Linux Administrator<br> Lingkup Kerja :
                            <ul>
                            <li>Menginstalasi sistem operasi Linux</li>
                            <li>Maintenance sistem operasi Linux</li>
                            <li>Melakukan virtualisasi</li>
                            </ul>
                            </li>
                            <li>Integrator Jaringan<br> Lingkup Kerja :
                            <ul>
                            <li>Merancang bangun dan menganalisa Wide Area Network</li>
                            <li>Memasang jaringan lokal</li>
                            <li>Melakukan instalasi perangkat jaringan berbasis luas (Wide Area Network)</li>
                            <li>Menginstalasi sistem operasi jaringan</li>
                            <li>Menganalisa dan memperbaiki kerusakan/kesalahan/tidak bekerjanya koneksi di sistem jaringan</li>
                            <li>Merancang web database untuk content server</li>
                            <li>Instalasi web server</li>
                            <li>Instalasi database server</li>
                            <li>Instalasi server jaringan</li>
                            <li>Instalasi content management sistem</li>
                            <li>Instalasi, integrasi software kolaborasi</li>
                            </ul>
                            </li>
                            <li>Web Administrator<br> Lingkup Kerja :
                            <ul>
                            <li>Konfigurasi web</li>
                            <li>Perawatan web</li>
                            <li>Memonitor jaringan</li>
                            <li>Mengatur traffic</li>
                            <li>Mendiagnosis permasalahan perangkat yang tersambung jaringan berbasis luas (Wide Area Network)</li>
                            <li>Melakukan perbaikan dan atau setting ulang koneksi jaringan berbasis luas (Wide Area Network)</li>
                            <li>Monitoring keamanan jaringan</li>
                            <li>Melakukan perbaikan dan atau setting ulang koneksi jaringan</li>
                            </ul>
                            </li>
                            </ol>			
						<h2 class="box-title mb-0 fw-500">Sejarah</h2>	
						<hr>
						<p class="fs-16 mb-30">
						SMK KarNas Sindangkasih didirikan pada tanggal 22 Januari 2013 dibawah naungan Yayasan Pendidkan Karya Nasional yang dipimpin oleh Bapak Drs. H. Dadang Djaka, BE. Smk Karnas sindangkasih pertama kali membuka dua jurusan yakni jurusan Rekayasa Perangkat lunak yang biasa disebut dengan RPL yang dimana jurusan itu menyangkut dengan teknologi IT dan yang kedua jurusan Teknik dan Bisnis Sepeda Motor yang biasa disebut dengan TBSm dimana jurusan itu menyangkut dengan berbagai macam kendaraan motor.
						</p>
						<p class="fs-16 mb-30">
						SMK KarNas Pertama kali beroperasi atau mulai menerima siswa-siswi pada tahun 2013 dimana pada saat itu hanya ada 26 orang siswa dan siswi diantaranya 16 siswa-siswi jurusan RPL dan 10 orang jurusan TBSm. Pada Saat itu SMK KarNas Sindangkasih baru memiliki 3 (tiga) lokal ruang pembelajaran yakni satu untuk bengkel produktif RPL, satu untuk bengkel produktif TBSM dan satu untuk ruang teori. adapun ruangan lain yaitu satu ruangan Kepala Sekolah, satu Ruangan Guru dan 1 ruangan TU (Tata Usaha).
						</p>
						<p class="fs-16 mb-30">
						Dari tahun ke tahun alhamdulillah SMK KarNas Sindangkasih semakin maju dengan bertambahnya siswa-siswi dan juga sarana prasarana, mulai dari peralatan bengkel TBSM, peralatan bengkel RPL,  ruangan teori, ruangan praktek dan lainnya.
						</p>
						<p class="fs-16 mb-30">
						SMK KarNas Sindangkasih merupakan sekolah yang berkomitment membekali siswa-siswinya dalam ilmu-ilmu sesuai dengan program studi, serta kemampuan dibidang teknologi, informasi, bahasa inggris, entrepreneur dan kepribadian yang unggul, serta terus berinovasi untuk menghasilkan lulusan yang memiliki kualifikasi tinggi dengan masa tunggu pendek yaitu segera memperoleh pekerjaan yang berkualitas ataupun berwiraswasta setelah lulus nanti.
						</p>
						<p class="fs-16 mb-30">
						Harapan lulusan SMK Karnas Sindangkasih Ciamis yaitu :
						
							Sudah memiliki keahlian.
							Santun, mampu dan mau bekerja keras.
							Sholeh, Mandiri, Kreatif.
							Sanggup melanjutkan kuliah.
						</p>
						
						<ul class="list list-mark">
                            <li> Sudah memiliki keahlian.</li>
                            <li> Santun, mampu dan mau bekerja keras.</li>
                            <li> Sholeh, Mandiri, Kreatif.</li>
                            <li> Sanggup melanjutkan kuliah.</li>
                        </ul>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</section>