  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
      <div class="content">
        <div class="col-lg-4 col-12">
				<div class="box">
				  <div class="text-white box-body bg-img text-center py-50" style="background-image: url(../images/gallery/creative/img-12.jpg);" data-overlay="5">
					<a href="#">
					  <img class="avatar avatar-xxl rounded-circle bg-warning-light" src="../images/avatar/avatar-16.png" alt="">
					</a>
					<h5 class="mt-2 mb-0"><a class="text-white" href="#">Maika Khalif</a></h5>
					<span>Designer</span>
				  </div>
				  <ul class="flexbox flex-justified text-center p-20">
					<li>
					  <span class="text-muted">Followers</span><br>
					  <span class="fs-20">6.6K</span>
					</li>
					<li class="be-1 bs-1 border-light">
					  <span class="text-muted">Following</span><br>
					  <span class="fs-20">5148</span>
					</li>
					<li>
					  <span class="text-muted">Tweets</span><br>
					  <span class="fs-20">2154</span>
					</li>
				  </ul>
				</div>
			  </div>

      </div>
      </div>
  </div>