<nav class="main-nav" role="navigation">

    <!-- Mobile menu toggle button (hamburger/x icon) -->
    <input id="main-menu-state" type="checkbox" />
    <label class="main-menu-btn" for="main-menu-state">
      <span class="main-menu-btn-icon"></span> Toggle main menu visibility
    </label>

    <!-- Sample menu definition -->

    <ul id="main-menu" class="sm sm-blue">
      <li class="<?php if($this->uri->uri_string() == 'admin/home') { echo 'current'; } ?>"><a href="<?php echo base_url('admin/home') ?>"><i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>Dashboard</a>
      </li>
      <li><a href="#"><i class="fa fa-list"><span class="path1"></span><span class="path2"></span></i>Master Data</a>
        <ul> 
          <li class="<?php if($this->uri->uri_string() == 'admin/master/DataUniversitas') { echo 'current'; } ?>"><a href="<?php echo base_url('admin/master/DataUniversitas') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Data Universitas</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/nilai/CetakNilai') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/nilai/CetakNilai') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Data Fakultas</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/nilai/CetakNilai') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/nilai/CetakNilai') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Data Jabatan</a></li>
        </ul>
      </li>

      <li><a href="#"><i class="fa fa-book"><span class="path1"></span><span class="path2"></span></i>Inquiry</a>				  
        <ul> 
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Jadwal') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Jadwal') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Buka Kelas</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/PaketKrs') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/PaketKrs') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Kirim Paket KRS</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/kprs') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/kprs') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>KPRS</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Presensi') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Presensi') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Presensi</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Nilai') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Nilai') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Isi Nilai</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/jadwal/uts') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/jadwal/uts') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Buka Jadwal UTS</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/jadwal/uas') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/jadwal/uas') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Buka jadwal UAS</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Presensi/Edit') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Presensi/Edit') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Edit Presensi</a></li>
        </ul>
      </li>  
      <li><a href="#"><i class="fa fa-info-circle"><span class="path1"></span><span class="path2"></span></i>Informasi</a>				  
        <ul> 
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Jadwal') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Jadwal') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Buka Kelas</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/PaketKrs') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/PaketKrs') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Kirim Paket KRS</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/kprs') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/kprs') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>KPRS</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Presensi') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Presensi') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Presensi</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Nilai') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Nilai') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Isi Nilai</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/jadwal/uts') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/jadwal/uts') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Buka Jadwal UTS</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/jadwal/uas') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/jadwal/uas') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Buka jadwal UAS</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Presensi/Edit') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Presensi/Edit') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Edit Presensi</a></li>
        </ul>
      </li>   
      <li><a href="#"><i class="fa fa-gears"><span class="path1"></span><span class="path2"></span></i>Setting Sistem</a>				  
        <ul> 
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/Jadwal') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/Jadwal') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Status Sistem</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/PaketKrs') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/PaketKrs') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Kontrol Sub-sistem</a></li>
          <li class="<?php if($this->uri->uri_string() == 'sirak/perkuliahan/PaketKrs') { echo 'current'; } ?>"><a href="<?php echo base_url('sirak/perkuliahan/PaketKrs') ?>"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Managemen pengguna</a></li>
        </ul>
      </li>   
    
    </ul>
</nav>  