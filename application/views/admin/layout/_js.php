	<!-- Vendor JS -->
	<script src="<?php echo base_url('assets');?>/admin/js/vendors.min.js"></script>
	<script src="<?php echo base_url('assets');?>/admin/js/pages/chat-popup.js"></script>
    <script src="<?php echo base_url('assets');?>/assets/icons/feather-icons/feather.min.js"></script>	

	<script src="<?php echo base_url('assets');?>/assets/vendor_components/apexcharts-bundle/dist/apexcharts.js"></script>
	<script src="<?php echo base_url('assets');?>/assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="<?php echo base_url('assets');?>/assets/vendor_components/fullcalendar/fullcalendar.js"></script>
	<script src="<?php echo base_url('assets');?>/assets/vendor_components/chart.js-master/Chart.min.js"></script>
	<script src="<?php echo base_url('assets');?>/assets/vendor_components/jquery-steps-master/build/jquery.steps.js"></script>
    <script src="<?php echo base_url('assets');?>/assets/vendor_components/sweetalert/sweetalert.min.js"></script>
	
	<script src="<?php echo base_url('assets');?>/assets/vendor_components/datatable/datatables.min.js"></script>
	<script src="<?php echo base_url('assets');?>/assets/vendor_components/raphael/raphael.min.js"></script>

	<script src="<?php echo base_url('assets');?>/assets/vendor_components/morris.js/morris.min.js"></script>
	<script src="<?php echo base_url('assets');?>/assets/vendor_components/sweetalert/sweetalert.min.js"></script>



	
	<!-- EduAdmin App -->
	<script src="<?php echo base_url('assets');?>/admin/js/jquery.smartmenus.js"></script>
	<script src="<?php echo base_url('assets');?>/admin/js/menus.js"></script>
	<script src="<?php echo base_url('assets');?>/admin/js/template.js"></script>
	<!-- <script src="<?php echo base_url('assets');?>/admin/js/pages/dashboard3.js"></script> -->
	<script src="<?php echo base_url('assets');?>/admin/js/pages/dashboard8.js"></script>
	<script src="<?php echo base_url('assets');?>/admin/js/pages/calendar.js"></script>
	<script src="<?php echo base_url('assets');?>/admin/js/pages/widget-charts2.js"></script>
    <script src="<?php echo base_url('assets');?>/admin/js/pages/steps.js"></script>
	<script src="<?php echo base_url('assets');?>/admin/js/pages/data-table.js"></script>



	