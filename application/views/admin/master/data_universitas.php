<div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->	  
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="me-auto">
					<h3 class="page-title">Data Universitas</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Home</li>
								<li class="breadcrumb-item active" aria-current="page">Data Universitas</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<section class="content">

        <div class="row">			  
				<div class="col-lg-8 col-12">
					  <div class="box">
						<div class="box-header with-border">
						  <h4 class="box-title">Edit</h4>
						</div>
						<!-- /.box-header -->
						<form class="form">
							<div class="box-body">
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
                                        <label class="form-label">Kode Universitas</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
									</div>
								  </div>
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Nama Universitas</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								</div>
								<div class="row">
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Alamat 1</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Alamat 2</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								</div>
                                <div class="row">
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Kota Universitas</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Kode POS</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								</div>
                                <div class="row">
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">No Telp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Faximil</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								</div>
                                <div class="row">
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Tanggal Akta SK Pendirian Akhir</label>
                                        <div class="input-group">
                                            <input type="date" class="form-control">
                                        </div>
                                    </div>
								  </div>
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Nomor Akta SK Pendirian Akhir</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								</div>
                                
                                <div class="row">
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Tanggal Awal Pendirian Universitas</label>
                                        <div class="input-group">
                                            <input type="date" class="form-control">
                                        </div>
                                    </div>
								  </div>
								</div>
                                <div class="row">
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <div class="input-group">
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
								  </div>
								  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Website</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
								  </div>
								</div>
								
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="button" class="btn btn-warning me-1">
								  <i class="ti-trash"></i> Cancel
								</button>
								<button type="submit" class="btn btn-primary">
								  <i class="ti-save-alt"></i> Save
								</button>
							</div>  
						</form>
					  </div>
					  <!-- /.box -->			
				</div>  

				<div class="col-lg-4 col-12">
                    <div class="box bs-3 border-warning">
                        <div class="box-header">
                            <h4 class="box-title"><strong>Info</strong></h4>
                        </div>
                        <div class="box-body">
                            Anda bisa melewati form ini jika mengambil jalur test (Klik Next)
                        </div>
                    </div>		
				</div>

				
		    </div>

		  <!-- /.box -->
		</section>
	  </div>
</div>