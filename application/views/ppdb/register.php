		<!---page Title --->
		<section class="bg-img pt-120 " data-overlay="4" style="background-image: url(<?php echo base_url('assets') ?>/images/front-end-img/background/bg-9.png">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="text-center">						
							<h2 class="page-title text-white">PPDB ONLINE</h2>
							<ol class="breadcrumb bg-transparent justify-content-center">
								<li class="breadcrumb-item"><a href="#" class="text-white-50"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item text-white active" aria-current="page">PPDB Online</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Content Header (Page header) -->	  
		<section class="py-50">
			<div class="container">
		
				<div class="box">
					<div class="box-header with-border">
					<h4 class="box-title">Formulir Siswa Baru</h4>
					<h6 class="box-subtitle">Silakah isi data diri dengan benar & lengkap</h6>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<!-- Nav tabs -->
						<div class="nav-tabs">
							<ul class="nav nav-tabs tabs-vertical" role="tablist">
								<li class="nav-item"> <a class="nav-link active" data-bs-toggle="tab" href="#home9" role="tab"><span><i class="ion-person me-15"></i>Data Siswa</span></a> </li>
								<li class="nav-item"> <a class="nav-link" data-bs-toggle="tab" href="#jurusan" role="tab"><span><i class="fa fa-paper-plane me-15"></i>Pilih Jurusan</span></a> </li>
								<li class="nav-item"> <a class="nav-link" data-bs-toggle="tab" href="#dataortu" role="tab"><span><i class="ion-email me-15"></i>Data Orang Tua</span></a> </li>
								<li class="nav-item"> <a class="nav-link" data-bs-toggle="tab" href="#kirimdata" role="tab"><span><i class="ion-email me-15"></i>Verifikasi & Kirim Data</span></a> </li>
							</ul>
							<!-- Tab panes -->
							<form action="<?php echo base_url('ppdb/Register/actionAdd')?>" method="POST">
							<div class="tab-content">
								<div class="tab-pane active" id="home9" role="tabpanel">
									<div class="p-15">
											<div class="row">
												<div class="col-sm-12 col-md-4">
													<div class="box ribbon-box">
														<div class="ribbon-two ribbon-two-primary"><span>Foto</span></div>
														<div class="box-header no-border p-0">				
															<a href="#">
															<img class="img-fluid" src="<?php echo base_url('assets') ?>/images/avatar/375x200/1.jpg" alt="">
															</a>
														</div>
														<div class="box-body">
																
															<div class="">
																<input type="file" name="f_photo" class="form-control"> 
																<code>Upload Pas Photo Berwarna 3x4 </code>
																<!-- <p class="text-fade  mx-auto">Format jpg, Max Upload 1mb </p> -->
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-8">
												<h4 class="box-title text-info mb-0 mt-15"><i class="ti-user me-15"></i> Data Diri</h4>
												<hr>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="lastName2" class="form-label">Nik :</label>
																<input type="text" name="nik" class="form-control" required> 
															</div>
														</div>
														
														
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="lastName2" class="form-label">Nama Lengkap:</label>
																<input type="text" name="name" class="form-control" required> 
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="emailAddress2" class="form-label">Email :</label>
																<input type="email" name="email" class="form-control" required> 
															</div>
														</div>
														
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="emailAddress2" class="form-label">Tempat :</label>
																<input type="text" name="tempat_lahir" class="form-control" required> 
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="phoneNumber2" class="form-label">Tanggal Lahir :</label>
																<input type="date" name="tanggal_lahir" class="form-control" required> 
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="emailAddress2" class="form-label">Jenis Kelamin :</label>
																<select class="form-select" name="jenis_kelamin" required>
																	<option value="">Pilih</option>
																	<option value="laki-laki">Laki-laki</option>
																	<option value="perempuan">Perempuan</option>
																</select>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="emailAddress2" class="form-label">Ukuran Baju :</label>
																<select class="form-select" name="size" >
																	<option value="">Pilih</option>
																	<option value="S">S</option>
																	<option value="M">M</option>
																	<option value="L">L</option>
																	<option value="XL">XL</option>
																	<option value="XXL">XXL</option>
																	<option value="XXXL">XXXL</option>
																</select>
															</div>
														</div>
													</div>
													<h4 class="box-title text-info mb-0 mt-15"><i class="fa fa-building me-15"></i> Data Sekolah</h4>
													<hr>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="lastName2" class="form-label">NISN :</label>
																<input type="number" name="nik" class="form-control" maxlength="10" required> 
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="emailAddress2" class="form-label">Asal Sekolah :</label>
																<select class="form-select" name="asal_sekolah" required>
																	<option value="">Pilih</option>
																	<option value="SMP 1">SMP 1</option>
																	<option value="SMP 2">SMP 2</option>
																</select>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label for="lastName2" class="form-label">Tahun Lulus :</label>
																<input type="year" name="nik" class="form-control" required> 
															</div>
														</div>
													</div>
													
												</div>					
											</div>
											
									</div>
								</div>
								<div class="tab-pane" id="jurusan" role="tabpanel">
									<div class="p-15">
										<div class="box">
											<div class="box-body">
												<h4 class="box-title text-info mb-0"> Silahkan lengkapi isian data berikut</h4>
												<hr class="my-15">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="form-label">Status Masuk</label>
															<select name="status_masuk" class="form-select">
																<option>Baru</option>
																<option>Pindahan</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="form-label">Pilihan 1</label>
																<select name="pilihan_1" class="form-select">
																	<option value="AKUTANSI DAN KEUANGAN LEMBAGA">AKUTANSI DAN KEUANGAN LEMBAGA</option>
																	<option value="TEKNIK AUDIO VIDEO">TEKNIK AUDIO VIDEO</option>
																	<option value="TEKNIK BISNIS SEPEDA MOTOR">TEKNIK BISNIS SEPEDA MOTOR</option>
																	<option value="TEKNIK KENDARAAN RINGAN DAN OTOMOTIF">TEKNIK KENDARAAN RINGAN DAN OTOMOTIF</option>
																	<option value="TEKNIK KOMPUTER DAN JARINGAN">TEKNIK KOMPUTER DAN JARINGAN</option>
																	<option value="TEKNIK PEMESINAN">TEKNIK PEMESINAN</option>
																	<option value="TEKNIK PENGELASAN">TEKNIK PENGELASAN</option>
																	<option value="TEKNIK PERBAIKAN BODY OTOMOTIF">TEKNIK PERBAIKAN BODY OTOMOTIF</option>
																</select>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="form-label">Pilihan 2</label>
																<select name="pilihan_2" class="form-select">
																	<option value="AKUTANSI DAN KEUANGAN LEMBAGA">AKUTANSI DAN KEUANGAN LEMBAGA</option>
																	<option value="TEKNIK AUDIO VIDEO">TEKNIK AUDIO VIDEO</option>
																	<option value="TEKNIK BISNIS SEPEDA MOTOR">TEKNIK BISNIS SEPEDA MOTOR</option>
																	<option value="TEKNIK KENDARAAN RINGAN DAN OTOMOTIF">TEKNIK KENDARAAN RINGAN DAN OTOMOTIF</option>
																	<option value="TEKNIK KOMPUTER DAN JARINGAN">TEKNIK KOMPUTER DAN JARINGAN</option>
																	<option value="TEKNIK PEMESINAN">TEKNIK PEMESINAN</option>
																	<option value="TEKNIK PENGELASAN">TEKNIK PENGELASAN</option>
																	<option value="TEKNIK PERBAIKAN BODY OTOMOTIF">TEKNIK PERBAIKAN BODY OTOMOTIF</option>
																</select>
														</div>
													</div>
												</div>
												
											</div>
											<!-- /.box-body -->
										</div>
										<!-- /.box -->	
									</div>
								</div>
								<div class="tab-pane" id="dataortu" role="tabpanel">
									<div class="p-15">
										<div class="row">
											<div class="col-lg-6 col-12">
												<div class="box">
														<div class="box-body">
															<h4 class="box-title text-info mb-0"><i class="ti-save me-15"></i> Data Ayah</h4>
															<hr class="my-15">
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Nama Ayah</label>
																		<input type="text" name="ayah_nama" class="form-control" placeholder="">
																	</div>
																</div>
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Alamat Orang Tua</label>
																		<input type="text" name="alamat_ortu" class="form-control" placeholder="">
																	</div>
																</div>
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Pekerjaan Ayah</label>
																		<select class="form-select" name="ayah_pekerjaan" id="" name="">
																			<option value="">Pilih</option>
																			<option value="Pegawai Negeri">Pegawai Negeri</option>
																			<option value="ABRI">ABRI</option>
																			<option value="Petani">Petani</option>
																			<option value="Pegawai Swasta">Pegawai Swasta</option>
																			<option value="Usaha Sendiri">Usaha Sendiri</option>
																			<option value="Tidak Bekerja">Tidak Bekerja</option>
																			<option value="Pensiunan">Pensiunan</option>
																			<option value="Buruh">Buruh</option>
																			<option value="Pedagang">Pedagang</option>
																			<option value="Wiraswasta">Wiraswasta</option>
																			<option value="Lain-lain">Lain-lain</option>
																		</select>
																	</div>
																</div>
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Pendidikan Ayah</label>
																		<select class="form-select" name="ayah_pendidikan" id="" >
																			<option value="">Pilih</option>
																			<option value="Tidak Tamat SD">Tidak Tamat SD</option>
																			<option value="Tamat SD">Tamat SD</option>
																			<option value="Tamat SMP">Tamat SMP</option>
																			<option value="Tamat SMA">Tamat SMA</option>
																			<option value="Tamat Diploma">Tamat Diploma</option>
																			<option value="Tamat Sarjana Muda">Tamat Sarjana Muda</option>
																			<option value="Tamat Sarjana">Tamat Sarjana</option>
																			<option value="Tamat Pasca Sarjana">Tamat Pasca Sarjana</option>
																			<option value="Lain-lain">Lain-lain</option>
																		</select>
																	</div>
																</div>
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Nomor Hp</label>
																		<input type="text" name="ayah_nohp" class="form-control" placeholder="">
																	</div>
																</div>
																
															</div>
														</div>
												</div>
											</div>
											<div class="col-lg-6 col-12">
												<div class="box">
														<div class="box-body">
															<h4 class="box-title text-info mb-0"><i class="ti-save me-15"></i> Data Ibu</h4>
															<hr class="my-15">
															<div class="row">
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Nama Ibu</label>
																		<input type="text" name="ibu_nama" class="form-control" placeholder="">
																	</div>
																</div>
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Pekerjaan Ibu</label>
																		<select class="form-select" id="" name="ibu_pekerjaan">
																		<option value="">Pilih</option>
																			<option value="Pegawai Negeri">Pegawai Negeri</option>
																			<option value="ABRI">ABRI</option>
																			<option value="Petani">Petani</option>
																			<option value="Pegawai Swasta">Pegawai Swasta</option>
																			<option value="Usaha Sendiri">Usaha Sendiri</option>
																			<option value="Tidak Bekerja">Tidak Bekerja</option>
																			<option value="Pensiunan">Pensiunan</option>
																			<option value="Buruh">Buruh</option>
																			<option value="IRT">IRT</option>
																			<option value="Pedagang">Pedagang</option>
																			<option value="Wiraswasta">Wiraswasta</option>
																			<option value="Lain-lain">Lain-lain</option>
																		</select>
																	</div>
																</div>
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Pendidikan Ibu</label>
																		<select class="form-select" id="" name="ibu_pendidikan">
																			<option value="">Pilih</option>
																			<option value="Tidak Tamat SD">Tidak Tamat SD</option>
																			<option value="Tamat SD">Tamat SD</option>
																			<option value="Tamat SMP">Tamat SMP</option>
																			<option value="Tamat SMA">Tamat SMA</option>
																			<option value="Tamat Diploma">Tamat Diploma</option>
																			<option value="Tamat Sarjana Muda">Tamat Sarjana Muda</option>
																			<option value="Tamat Sarjana">Tamat Sarjana</option>
																			<option value="Tamat Pasca Sarjana">Tamat Pasca Sarjana</option>
																			<option value="Lain-lain">Lain-lain</option>
																		</select>
																	</div>
																</div>
																<div class="col-md-12">
																	<div class="form-group">
																		<label class="form-label">Nomor Hp</label>
																		<input type="text"name="ibu_nohp" class="form-control" placeholder="">
																	</div>
																</div>
																
															</div>
														</div>
												</div>
											</div>

											
										</div>	
									</div>
								</div>
								<div class="tab-pane" id="kirimdata" role="tabpanel">
									<div class="p-15">
										<div class="box box-body" >
											<div class="form-group">
												<div class="c-inputs-stacked">
													<input type="checkbox" id="checkme">
													<label for="checkme" class="d-block">Data yang kami input adalah benar dan dapat diverifikasi untuk dipertanggungjawabkan </label>
													<div class=" pt-20">
														<button class="waves-effect waves-light btn btn-app btn-primary" id="kirim" disabled="disabled">Kirim</button>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					</form>

					<!-- /.box-body -->
				</div>
				<div class="box box-body">
				</div>
					
			</div>
		</section>

