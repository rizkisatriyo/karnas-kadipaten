<!---page Title --->
		<section class="bg-img pt-120 " data-overlay="4" style="background-image: url(<?php echo base_url('assets') ?>/images/front-end-img/background/bg-9.png">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="text-center">						
							<h2 class="page-title text-white">PPDB ONLINE</h2>
							<ol class="breadcrumb bg-transparent justify-content-center">
								<li class="breadcrumb-item"><a href="#" class="text-white-50"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item text-white active" aria-current="page">PPDB Online</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Content Header (Page header) -->	  
		<section class="py-130">
			<div class="container">
				<div class="box pull-up">
					<div class="box-body bg-img" style="background-image: url(<?php echo base_url('assets') ?>/images/bg-5.png);" data-overlay-light="9">
						<div class="d-lg-flex align-items-center justify-content-between">
							<div class="d-md-flex align-items-center mb-30 mb-lg-0 w-p100">
								<img src="<?php echo base_url('assets') ?>/images/svg-icon/color-svg/custom-14.svg" class="img-fluid max-w-150" alt="" />
								<div class="ms-30">
									<h4 class="mb-10">Data Berhasil Dikirim !</h4>
									<p class="mb-0 text-fade">Data akan diverifikasi oleh sekolah <br> </p>
								</div>
							</div>
							<div>
								<button type="button" class="waves-effect waves-light w-p100 btn btn-primary" style="white-space: nowrap;">Kembali</button>
							</div>
						</div>							
					</div>
				</div>
			</div>
		</section>
				
