		<script src="<?php echo base_url('assets');?>/theme/plugins/respond.min.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<!--<![endif]-->
		<script src="<?php echo base_url('assets');?>/theme/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo base_url('assets');?>/theme/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="<?php echo base_url('assets');?>/theme/js/login.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Login.init();
			});
		</script>