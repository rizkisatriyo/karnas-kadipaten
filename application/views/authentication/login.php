
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/favicon.ico">

    <title>SILAKAN KAWAN - Log in </title>
  
    <!-- Vendors Style-->
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/main/css/vendors_css.css">
      
    <!-- Style-->  
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/main/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/main/css/skin_color.css">   

</head>
<body class="hold-transition theme-primary bg-img" style="background-image: url(<?php echo base_url('assets');?>/images/auth-bg/bg-11.jpg)">
    
    <div class="container h-p100">
        <div class="row align-items-center justify-content-md-center h-p100">   
            <div class="col-12">
                <div class="row justify-content-center no-gutters">
                    <div class="col-lg-5 col-md-5 col-12">
                        <div class="bg-white rounded30 shadow-lg">
                            <div class="content-top-agile p-20 pb-0">
				                <span class="light-logo"><img src="<?php echo base_url('assets')?>/images/logo-stikesypib.png" alt="logo" width="150px"></span>

                                <h2 class="text-primary">Universitas YPIB </h2>
                                <p class="mb-0">Sistem Login Satu Pintu</p>                         
                            </div>
                            <div class="p-40">
                                <b class="text-danger"><?php echo validation_errors(); ?></b>
                                <form method="post" action="<?php echo base_url('auth/login'); ?>" role="login">
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
                                            </div>
                                            <input type="email" name="email" class="form-control pl-15 bg-transparent" placeholder="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
                                            </div>
                                            <input type="password" name="password" class="form-control pl-15 bg-transparent" placeholder="Password">
                                        </div>
                                    </div>
                                      <div class="row">
                                        <div class="col-6">
                                          <div class="checkbox">
                                            <input type="checkbox" id="basic_checkbox_1" >
                                            <label for="basic_checkbox_1">Remember Me</label>
                                          </div>
                                        </div>
                                        <!-- /.col -->
                                        <!-- <div class="col-6">
                                         <div class="fog-pwd text-right">
                                            <a href="javascript:void(0)" class="hover-warning"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
                                          </div>
                                        </div> -->
                                        <!-- /.col -->
                                        <div class="col-12 text-center">
                                          <button type="submit" class="btn btn-danger mt-10" name="submit" value="login">SIGN IN</button>
                                        </div>
                                        <!-- /.col -->
                                      </div>
                                </form> 
                                <div class="text-center">
                                    <p class="mt-15 mb-0"><a href="<?php echo base_url();?>ponakan/Auth/register" class="text-warning ml-5">Lupa Password?  </a></p>
                                </div>  
                            </div>                      
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Vendor JS -->
    <script src="<?php echo base_url('assets');?>/theme/main/js/vendors.min.js"></script>
    <script src="<?php echo base_url('assets');?>/theme/main/js/pages/chat-popup.js"></script>
    <script src="<?php echo base_url('assets');?>/theme/assets/icons/feather-icons/feather.min.js"></script>    

</body>
</html>
