<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Website extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        
    }

    public function index()
    {
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_PORT           => "443",
			CURLOPT_URL            => "https://siapschool.com:6365/api_slide/data_main/?sekolah_id=".sekolah_id(),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "GET",
			CURLOPT_POSTFIELDS     => "",
			CURLOPT_HTTPHEADER     => array(
				"Postman-Token: 6ee01772-ce40-4ded-919a-1cc783b3027f",
				"cache-control: no-cache",
			),
		));

		$resp = curl_exec($curl);
		$err  = curl_error($curl);

		curl_close($curl);
		echo $resp;
		
        // $curl = curl_init();

		// 		curl_setopt_array($curl, array(
		// 			CURLOPT_PORT           => "443",
		// 			CURLOPT_URL            => base_url_api()."api_news/data/?sekolah_id=".sekolah_id(),
		// 			CURLOPT_RETURNTRANSFER => true,
		// 			CURLOPT_ENCODING       => "",
		// 			CURLOPT_MAXREDIRS      => 10,
		// 			CURLOPT_TIMEOUT        => 30,
		// 			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
		// 			CURLOPT_CUSTOMREQUEST  => "GET",
		// 			CURLOPT_POSTFIELDS     => "",
		// 			CURLOPT_HTTPHEADER     => array(
		// 				"Postman-Token: 6ee01772-ce40-4ded-919a-1cc783b3027f",
		// 				"cache-control: no-cache",
		// 			),
		// 		));

		// 		$resp = curl_exec($curl);
		// 		$err  = curl_error($curl);

		// 		curl_close($curl);
                

		// 		if ($err) {
		// 			echo "cURL Error #:" . $err;
		// 		} else {
		// 			$data = json_decode($resp,true);
		// 			$data['data_slide'] = $data['data_slide'];
		// 			$data['data_news'] = $data['data_news'];
		// 			$data['data_testimoni'] = $data['data_testimoni'];
		// 			$data['page'] = "home";
		// 			$data['name'] = 'BERANDA';
		// 			$data['name_sekolah'] = name_sekolah();
		// 			$data['color_dominan'] = color_sekolah();

        //             $this->template->load('website/layout/template', 'website/index', $data);

		// 			// $this->load->view('head', $data);
		// 			// $this->load->view('home', $data);
		// 			// $this->load->view('footer', $data);
			


					
		// 		}
       

    }
    public function profil()
    {
       
        $this->template->load('website/layout/template', 'website/profil');

    }
    public function blog()
    {

		
		$curl = curl_init();

	
		curl_setopt_array($curl, array(
			CURLOPT_PORT           => "443",
			CURLOPT_URL            => "https://siapschool.com:6365/api_news/data/?sekolah_id=".sekolah_id(),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "GET",
			CURLOPT_POSTFIELDS     => "",
			CURLOPT_HTTPHEADER     => array(
				"Postman-Token: 6ee01772-ce40-4ded-919a-1cc783b3027f",
				"cache-control: no-cache",
			),
		));

		$resp = curl_exec($curl);
		$err  = curl_error($curl);

		curl_close($curl);
		echo $resp;

		// if ($err) {
		// 	echo "cURL Error #:" . $err;
		// } else {
		// 	//print_r(json_decode($resp,true));
		// 	$data = json_decode($resp,true);
		// 	// print_r($data);
		// 	$data['data'] = $data['data'];
		// 	$data['name'] = 'Berita';
		// 	$data['page'] = "page";
		// 	$data['name_sekolah'] = name_sekolah();
		// 	$data['color_dominan'] = color_sekolah();
		// 	$this->template->load('website/layout/template', 'website/blog_list',$data);


		// 	// $this->load->view('head', $data);
		// 	// $this->load->view('berita', $data);
		// 	// $this->load->view('footer', $data);
	
			
		// }

    }
    public function blogDetail()
    {
		$curl = curl_init();

	
		curl_setopt_array($curl, array(
			// CURLOPT_PORT           => "80",
			CURLOPT_URL            => "https://siapschool.com:6350/api_news/data/?sekolah_id=".sekolah_id(),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "GET",
			CURLOPT_POSTFIELDS     => "",
			CURLOPT_HTTPHEADER     => array(
				"Postman-Token: 6ee01772-ce40-4ded-919a-1cc783b3027f",
				"cache-control: no-cache",
			),
		));

		$resp = curl_exec($curl);
		$err  = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$data = json_decode($resp,true);
			$data['data'] = $data['data'];
			$data['page'] = "page";
			$data['name'] = 'Detail Berita';
			$data['name_sekolah'] = name_sekolah();
			$data['color_dominan'] = color_sekolah();
			// $this->load->view('head', $data);
			// $this->load->view('detail_berita', $data);
			// $this->load->view('footer', $data);
			$this->template->load('website/layout/template', 'website/blog_detail', $data);

			
		}

    }
    public function faqs()
    {
       
        $this->template->load('website/layout/template', 'website/faq');

    }
    public function contactUs()
    {
       
        $this->template->load('website/layout/template', 'website/contact_us');

    }

	public function rpl()
	{
        $this->template->load('website/layout/template', 'website/rpl');
	}
	public function tsm()
	{
        $this->template->load('website/layout/template', 'website/tsm');
	}
	public function tkj()
	{
        $this->template->load('website/layout/template', 'website/tkj');
	}
}
