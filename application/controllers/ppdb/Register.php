<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Register extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->check_login();
        // if ($this->session->userdata('id_role') != "2") {
        //     redirect('', 'refresh');
        // }
        //        $this->load->model('Proposal_model');
        //        $this->load->helper('string');

    }

    public function index()
    {


        $this->template->load('website/layout/template', 'ppdb/register');
    }

    public function actionAdd()
        {
            $curl = curl_init();

            $curl_parameters = array(
                'nik'               => $this->input->post('nik'),
                'name'              => $this->input->post('name'),
                'jenis_kelamin'     => $this->input->post('jenis_kelamin'),
                'email'             => $this->input->post('email'),
                'no_hp'             => $this->input->post('no_hp'),
                'tempat_lahir'      => $this->input->post('tempat_lahir'),
                'tanggal_lahir'     => $this->input->post('tanggal_lahir'),
                'alamat'            => $this->input->post('alamat'),
                'size'              => $this->input->post('size'),
                'asal_sekolah'      => $this->input->post('asal_sekolah'),
                'status_masuk'      => $this->input->post('status_masuk'),
                'pilihan_1'         => $this->input->post('pilihan_1'),
                'pilihan_2'         => $this->input->post('pilihan_2'),
                'ayah_nama'         => $this->input->post('ayah_nama'),
                'ayah_pekerjaan'    => $this->input->post('ayah_pekerjaan'),
                'ayah_pendidikan'   => $this->input->post('ayah_pendidikan'),
                'ayah_nohp'         => $this->input->post('ayah_nohp'),
                'alamat_ortu'       => $this->input->post('alamat_ortu'),
                'ibu_nama'          => $this->input->post('ibu_nama'), 
                'ibu_pekerjaan'     => $this->input->post('ibu_pekerjaan'),  
                'ibu_pendidikan'    => $this->input->post('ibu_pendidikan'),
                'ibu_nohp'          => $this->input->post('ibu_nohp'),
                'f_photo'           => $this->input->post('f_photo'),
                'f_id_sekolah'      => sekolah_id(),
              );
            
                    curl_setopt_array($curl, array(
                        // CURLOPT_PORT           => "80",
                        CURLOPT_URL            => "http://siapschool.com:6365/api_ppdb/insert",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING       => "",
                        CURLOPT_MAXREDIRS      => 10,
                        CURLOPT_TIMEOUT        => 30,
                        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST  => "POST",
                        CURLOPT_POSTFIELDS     => http_build_query( $curl_parameters ),
                        CURLOPT_HTTPHEADER     => array(
                            "Content-Type: application/x-www-form-urlencoded",
                            "Cookie: connect.sid=s%3ALYczmJPmG1eNN8XGkHtpG55eQBjTgemW.aaaSa9P1IBHwncjaw6ZJX%2FfbzGtQJTwBZGuaQvpwWGI",
                        ),
                    ));
    
                    $resp = curl_exec($curl);
                    $err  = curl_error($curl);
    
                    curl_close($curl);
            // $this->formAdd($pesan);
            redirect('ppdb/register/success', 'refresh');
        }

    public function Success()
    {
        $this->template->load('website/layout/template', 'ppdb/success');

    }
    
}
