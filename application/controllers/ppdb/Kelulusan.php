<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kelulusan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        
    }

    public function index()
    {
       
        $this->template->load('layout/template', 'pmb/kelulusan');

    }
}
